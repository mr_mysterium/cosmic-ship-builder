package model;


import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.WorldManifold;
import com.badlogic.gdx.utils.Array;
import com.hgs.ShipBuilder;
import part.Part;

/**
 * Die Klasse Player dient als �bergeordneter Controller der die logischen und physikalischen Elemente des Spielers vereint
 */
public class Player {
	
	/** Die lebendigkeit des Spielers */
	boolean alive;
	
	/** Zugriff auf die Hauptinstanz des spiels*/
	ShipBuilder game;
	
	/** Instanz des Algorithmus zur �berpr�fung des Zusammengangs */
	ConnectedAlg con;
	
	/** Die physikalische Welt von Box2d */
	World w;
	
	/** Das Schiff das Statuswerte und die logische Darstellung aller Teile enth�lt */
	Ship ship;
	
	/** Die logische Darstellung aller Teile in Integerform zur verwendung in {@link ConnectedAlg} */
	int[][]partgraph;
	
	/** Die physikalische Darstellung des Schiffs*/
	Blueprint blueprint;
	
	/** Der Spielerinput zur Steurung */
	int steeringinput = 0;
	
	/** Der Spielerinput zur Beschleunigung */
	int thrustinput = 0;
	
	/**
	 * Erzeugt einen neuen Spieler und altualisiert die n�tigegen Daten wie das Schiff 
	 *
	 * @param sb Die Hauptinstanz des Spiels
	 * @param w Die physikalische Welt von Box2d
	 * @param modelbase logische Darstellung auf der das Schiff basiert
	 */
	public Player(ShipBuilder sb,World w,Part[][] modelbase) {
		this.game = sb;
		con = new ConnectedAlg();
		this.w= w;
		ship = new Ship("");
		ship.update(modelbase);
		blueprint = new Blueprint(sb,w, ship.getShipmodel());
		partgraph= new int[32][32];
		updatepartgraph();
		alive = true;
	}
	
	/**
	 * Methode die bei einer Asteroidenkollision aufgerufen wird
	 * Kinetische Energieberechnung sowie Schadensberechnung 
	 * Abspielung der Kollisionseffekte
	 * �berpr�fen ob die Lebenspunkte 0 unterschritten haben
	 * @param speed die Kollisionsgeschwindigkeit
	 * @param asteroidmass Die Masse des Asteroiden
	 * @param worldManifold Die Liste an stellen an denen kollidiert wurde
	 * @param fixture Der kollidierte Teilk�rper
	 */
	public void collide(Vector2 speed, float asteroidmass,WorldManifold worldManifold, Fixture fixture){
		float collisionspeed = (float) Math.sqrt(Math.pow(speed.x, 2)+Math.pow(speed.y, 2));
		float kineticenergy = (float) (1/10f* asteroidmass * Math.pow(collisionspeed, 2));
		float damage = (float) kineticenergy/200;
		ShipBuilder.effects.collisionsound.play(damage/50);
		ShipBuilder.effects.spawnparticle("collision",worldManifold.getPoints()[0].x,worldManifold.getPoints()[0].y,(float) Math.sqrt(damage/1000));
		ArrayList<Fixture>destructor= new ArrayList<Fixture>();
		UserData userdata = (UserData) fixture.getUserData();
		if (ship.getShipmodel()[(int) userdata.metadata.x][(int) userdata.metadata.y].modifyHp(-damage)) {distributedamage(destructor,fixture);
		if (ship.getShipmodel()[(int) userdata.metadata.x][(int) userdata.metadata.y].getPart().equals("kern"))kill();
		}
		game.gs.setTodestroy(destructor);
		}
		
		
	
	
	/**
	 * Den Spieler als nichtmehr Lebendig setzten
	 */
	private void kill() {
		alive=false;
		
	}

	/**
	 * Das hinzuf�gen des kaputten Teiles zur Liste der zu zerst�renden Fixtures
	 *
	 * @param d Liste der kapputen Teile
	 * @param fix Der zu zerst�rende Teilk�rper
	 */
	private void distributedamage(ArrayList<Fixture>d,Fixture fix) {
		d.add(fix);
		
		
	}

	/**
	 * Gebe das Schiff des Spielers zur�ck
	 *
	 * @return das Schiff
	 */
	public Ship getShip() {
		return ship;
	}
	
	/**
	 * Setze das Schiff des Spielers
	 *
	 * @param ship dass zu setzende Schiff
	 */
	public void setShip(Ship ship) {
		this.ship = ship;
	}
	
	/**
	 * gebe das physikalische Modell zur�ck
	 *
	 * @return das physikalische Modell
	 */
	public Blueprint getblueprint() {
		return blueprint;
	}
	
	/**
	 * Setze das physikalische Modell
	 *
	 * @param bp das physikalische Modell
	 */
	public void setblueprint(Blueprint bp) {
		this.blueprint = bp;
	}
	
	/**
	 * Setze den Spielerinput f�r die Steurung
	 *
	 * @param steeringinput der Steuerungsinput
	 */
	public void setSteeringinput(int steeringinput) {
		this.steeringinput = steeringinput;
	}
	
	/**
	 * Setze den Beschleunigungsinput
	 *
	 * @param thrustinput Beschleunigungsinput
	 */
	public void setThrustinput(int thrustinput) {
		this.thrustinput = thrustinput;
	}
	
	/**
	 * gebe den Steurungsinput zur�ck
	 *
	 * @return der Steuerungsinput
	 */
	public int getSteeringinput() {
		return steeringinput;
	}
	
	/**
	 * gebe den Beschleunigungsinput zur�ck
	 *
	 * @return der Beschleunigungsinput
	 */
	public int getThrustinput() {
		return thrustinput;
	}

	/**
	 * �berpr�fe ob der Spieler lebt
	 *
	 * @return der Lebenszustand
	 */
	public boolean isAlive() {
		return alive;
	}

	/**
	 * Setzt den Lebenszustand
	 *
	 * @param der Lebenszustand
	 */
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	
	/**
	 * Aktualisiere Logische Darstellung des Schiffes als Integermodell
	 */
	public void updatepartgraph(){
		for (int i = 0; i < ship.getShipmodel().length; i++) {
			for (int j = 0; j < ship.getShipmodel()[i].length; j++) {
				if (ship.getShipmodel()[j][i] != null) {
					partgraph[j][i]=1;
				}else {
					partgraph[j][i]=0;
				}
			}
		}
	}

	/**
	 * Gebe logische Darstellung des Schiffes als Integermodell zur�ck
	 *
	 * @return the partgraph
	 */
	public int[][] getPartgraph() {
		return partgraph;
	}
	
	/**
	 * Gebe das Integermodell in der Konsole
	 */
	public void printPartgraph() {
		for (int i = 0; i < partgraph.length; i++) {
			for (int j = 0; j < partgraph[i].length; j++) {
				System.out.print(partgraph[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("\n"+"\n"+"\n");
	}
	
}
