package model;
import part.Part;


/**
 * Die Klasse schiff weist die Stautswerte des SPielers auf und besteht größtenteils aus getter und Settermethoden, diese werde aus redundanz nicht weiter ausgeführt.
 */
public class Ship {

/** The name. */
String name;

/** The shipmodel. */
Part[][] shipmodel;
 
 /** The parts. */
 int parts=0;
 
 /** The partsmax. */
 int partsmax=1;
 
 /** The hp. */
 float hp = 0;
 
 /** The shield. */
 int shield=0;
 
 /** The mass. */
 int mass=0;
 
 /** The speed. */
 double speed;
 
 /** The thrust. */
 double thrust;
 
 /** The breakthrust. */
 double breakthrust;
 
 /** The angularthrust. */
 double angularthrust;
 
 /** The accelaration. */
 double accelaration=0;
 
 /** The decelaration. */
 double decelaration;
 
 /** The rotationspeed. */
 double rotationspeed;
 
 /** The maxspeed. */
 double maxspeed = 0;
 
 /** The energyproduction. */
 int energyproduction = 0;
 
 /** The battery. */
 int battery=0;
 
 /** The energyconsumption. */
 int energyconsumption=0;
 
 /** The crew. */
 int crew=0;
 
 /** The crewneed. */
 int crewneed=0;
 
 /**
  * Instantiates a new ship.
  *
  * @param name the name
  */
 public Ship(String name) {
	this.name = name;
	shipmodel = new Part[32][32];
}

/**
 * Gets the name.
 *
 * @return the name
 */
public String getName() {
	return name;
}

/**
 * Gets the parts.
 *
 * @return the parts
 */
public int getParts() {
	return parts;
}

/**
 * Gets the partsmax.
 *
 * @return the partsmax
 */
public int getPartsmax() {
	return partsmax;
}

/**
 * Gets the hp.
 *
 * @return the hp
 */
public float getHp() {
	return hp;
}

/**
 * Gets the shield.
 *
 * @return the shield
 */
public int getShield() {
	return shield;
}

/**
 * Gets the mass.
 *
 * @return the mass
 */
public int getMass() {
	return mass;
}

/**
 * Gets the accelaration.
 *
 * @return the accelaration
 */
public double getAccelaration() {
	return accelaration;
}

/**
 * Gets the maxspeed.
 *
 * @return the maxspeed
 */
public double getMaxspeed() {
	return maxspeed;
}

/**
 * Gets the energyproduction.
 *
 * @return the energyproduction
 */
public int getEnergyproduction() {
	return energyproduction;
}

/**
 * Gets the battery.
 *
 * @return the battery
 */
public int getBattery() {
	return battery;
}

/**
 * Gets the energyconsumption.
 *
 * @return the energyconsumption
 */
public int getEnergyconsumption() {
	return energyconsumption;
}

/**
 * Gets the crew.
 *
 * @return the crew
 */
public int getCrew() {
	return crew;
}

/**
 * Gets the crewneed.
 *
 * @return the crewneed
 */
public int getCrewneed() {
	return crewneed;
}

/**
 * Sets the parts.
 *
 * @param parts the new parts
 */
public void setParts(int parts) {
	this.parts = parts;
}

/**
 * Sets the partsmax.
 *
 * @param partsmax the new partsmax
 */
public void setPartsmax(int partsmax) {
	this.partsmax = partsmax;
}

/**
 * Sets the hp.
 *
 * @param hp the new hp
 */
public void setHp(float hp) {
	this.hp = hp;
}

/**
 * Sets the shield.
 *
 * @param shield the new shield
 */
public void setShield(int shield) {
	this.shield = shield;
}

/**
 * Sets the mass.
 *
 * @param mass the new mass
 */
public void setMass(int mass) {
	this.mass = mass;
}

/**
 * Sets the accelaration.
 *
 * @param accelaration the new accelaration
 */
public void setAccelaration(double accelaration) {
	this.accelaration = accelaration;
}

/**
 * Sets the maxspeed.
 *
 * @param maxspeed the new maxspeed
 */
public void setMaxspeed(double maxspeed) {
	this.maxspeed = maxspeed;
}

/**
 * Sets the energyproduction.
 *
 * @param energyproduction the new energyproduction
 */
public void setEnergyproduction(int energyproduction) {
	this.energyproduction = energyproduction;
}

/**
 * Sets the battery.
 *
 * @param battery the new battery
 */
public void setBattery(int battery) {
	this.battery = battery;
}

/**
 * Sets the energyconsumption.
 *
 * @param energyconsumption the new energyconsumption
 */
public void setEnergyconsumption(int energyconsumption) {
	this.energyconsumption = energyconsumption;
}

/**
 * Sets the crew.
 *
 * @param crew the new crew
 */
public void setCrew(int crew) {
	this.crew = crew;
}

/**
 * Sets the crewneed.
 *
 * @param crewneed the new crewneed
 */
public void setCrewneed(int crewneed) {
	this.crewneed = crewneed;
}

/**
 * Update.
 *
 * @param model the model
 */
public void update(Part[][]model) {
	this.shipmodel = model;
	resetstats();
	calcstats();
}

/**
 * Gets the thrust.
 *
 * @return the thrust
 */
public double getThrust() {
	return thrust;
}


/**
 * Sets the thrust.
 *
 * @param thrust the new thrust
 */
public void setThrust(double thrust) {
	this.thrust = thrust;
}



/**
 * Gets the breakthrust.
 *
 * @return the breakthrust
 */
public double getBreakthrust() {
	return breakthrust;
}

/**
 * Sets the breakthrust.
 *
 * @param breakthrust the new breakthrust
 */
public void setBreakthrust(double breakthrust) {
	this.breakthrust = breakthrust;
}


/**
 * Gets the angularthrust.
 *
 * @return the angularthrust
 */
public double getAngularthrust() {
	return angularthrust;
}

/**
 * Sets the angularthrust.
 *
 * @param angularthrust the new angularthrust
 */
public void setAngularthrust(double angularthrust) {
	this.angularthrust = angularthrust;
}

/**
 * Gets the decelaration.
 *
 * @return the decelaration
 */
public double getDecelaration() {
	return decelaration;
}

/**
 * Sets the decelaration.
 *
 * @param decelaration the new decelaration
 */
public void setDecelaration(double decelaration) {
	this.decelaration = decelaration;
}

/**
 * Gets the rotationspeed.
 *
 * @return the rotationspeed
 */
public double getRotationspeed() {
	return rotationspeed;
}

/**
 * Sets the rotationspeed.
 *
 * @param rotationspeed the new rotationspeed
 */
public void setRotationspeed(double rotationspeed) {
	this.rotationspeed = rotationspeed;
}

/**
 * Gets the speed.
 *
 * @return the speed
 */
public double getSpeed() {
	return speed;
}

/**
 * Sets the speed.
 *
 * @param speed the new speed
 */
public void setSpeed(double speed) {
	this.speed = speed;
}

/**
 * Resetstats.
 */
private void resetstats() {
	  parts=0;
	  partsmax=1;
	  hp = 0;
	  shield=0;
	  mass=0;
	  thrust=0;
	  breakthrust=0;
	  angularthrust=0;
	  accelaration=0;
	  maxspeed = 0;
	  decelaration =0;
	  rotationspeed=0;
	  energyproduction = 0;
	  battery=0;
	  energyconsumption=0;
	  crew=0;
	  crewneed=0;
}

	/**
	 * Calcstats.
	 */
	private void calcstats() {
		for (Part[] partsy : shipmodel) {
			for (Part partx : partsy) {
				if (partx != null) {
					
					partx.add(this,partx.getMaterial());
					parts++;
				}

			}

		}
		if (parts>0) {
			setAccelaration(this.getThrust()/this.getMass());	
			setDecelaration(this.getBreakthrust()/this.getMass());
			setRotationspeed(this.getAngularthrust()/this.getMass());
		}else {
			setAccelaration(0);
			setDecelaration(0);
			setRotationspeed(0);
		}
		
		setCrewneed(parts/4+crewneed);
		setMaxspeed(getAccelaration()*4);
	}

	/**
	 * Gets the shipmodel.
	 *
	 * @return the shipmodel
	 */
	public Part[][] getShipmodel() {
		return shipmodel;
	}

	/**
	 * Sets the shipmodel.
	 *
	 * @param shipmodel the new shipmodel
	 */
	public void setShipmodel(Part[][] shipmodel) {
		this.shipmodel = shipmodel;
	}
}

