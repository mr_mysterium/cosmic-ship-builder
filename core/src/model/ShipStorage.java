package model;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.hgs.ShipBuilder;

import Scenes.PartMenu;
import part.Antrieb;
import part.Batterie;
import part.Dreieck;
import part.Generator;
import part.Kern;
import part.Panzerung;
import part.Part;
import part.Quartiere;
import part.Schildgenerator;
import part.Spitze;
import part.Steuerung;
import part.Turret;


/**
 * Die speicherung des Schiffes wird in dieser Klasse vorgenommen
 */
public class ShipStorage{
	
	/** Die Hauptinstanz des Spiels*/
	ShipBuilder sb;
	
	/** Liste der zu speichernden/ladenden schiffe */
	FileHandle[] ships;
	
	/** Zu ladende Datei */
	FileHandle shiphandle;
	
	/** vorl�ufige Beh�lter in den die Teile aus der Datei extrahiert werden */
	Part[][]shipload;
	
	/** Das Partmen� */
	PartMenu pm;
	
	/**
	 * Erzeugt  neuen Schiffspeicher
	 *
	 * @param sb Die Hauptinstanz des Spiels
	 */
	public ShipStorage(ShipBuilder sb) {
		this.sb = sb;
		pm = new PartMenu(sb);
		ships = Gdx.files.local("ships/").list();
	}
	
	/**
	 * Das abspeichern des Schiffs und das darauf folgende aktualisieren der Schiffsliste im editor
	 */
	public void saveship() {
		shiphandle = Gdx.files.local("ships/"+sb.es.ship.getName()+".ship");
		writeship();
		update(sb.es.browser.shiplist);
	}
	
	/**
	 * Das aktualisieren der schiffliste
	 *
	 * @param l the l
	 */
	public void update(List<String> l){
		ships = Gdx.files.local("ships/").list();
	 int entries =0;
	 		for(FileHandle file: ships) {
		   entries++;
		}
		String[] names= new String[entries];
		for (int i = 0; i < entries; i++) {
			names[i]=ships[i].nameWithoutExtension();
			
		}
		l.setItems(names);
	}
	
	  
	/**
	 * Das Schrittweise niederschreiben einses Schiffes in eine Datei
	 */
	void writeship() {
		  String data="";
		  for (int i = 0; i < sb.es.getParts().length; i++) {
				for (int j = 0; j < sb.es.getParts()[i].length; j++) {
					if (sb.es.getParts()[i][j] != null) {
						data = data+(sb.es.getParts()[i][j].getPart()+","+sb.es.getParts()[i][j].getMaterial()+","+ (int)sb.es.getParts()[i][j].getRotation()+System.getProperty("line.separator"));
					}else {
						data= data+("null"+System.getProperty("line.separator"));
					}
				}
			}
		  shiphandle.writeString(data, false);
	  }
	
	/**
	 * Das setzen eines Schiffes durch das laden einer Datei
	 */
	public void shipset() {
		  loadship(sb.es.browser.getSelected());
		  if (sb.es.ship!=null) {
				sb.es.ship = new Ship(sb.es.browser.getSelected().nameWithoutExtension());
			}
			sb.es.ship.update(shipload);
			sb.es.setParts(shipload);
			sb.es.updatepartgraph();
			sb.es.getSi().updateinfo(sb.es.ship);
			saveship();
	  }
	  
	  
	  /**
  	 * Das Laden einer Schiffsdatei
  	 *
  	 * @param fh Die Schiffsdatei
  	 * @return the Das Modell des Schiffes
  	 */
  	public Part[][] loadship(FileHandle fh) {
			String[] lines = readship(fh).split(System.getProperty("line.separator"));
		shipload= new Part[32][32];
			for (int i = 0; i < shipload.length; i++) {
				for (int j = 0; j < shipload[i].length; j++) {
					if (lines[32*i+j].contains("null")) {
						shipload[i][j]= null;
					}else {			
						String[]args=lines[32*i+j].split(",");
						String argpart = args[0]; 
						int argmaterial = Integer.parseInt(args[1]);
						int argrot = Integer.parseInt(args[2]);
						for (int k = 0;k<=10; k++) {
							if (argpart.equals(pm.parttypes[k])) {
								switch (k) {
								case 0:
									shipload[i][j] = new Antrieb(pm,sb.es.ship, argpart,argmaterial,i,j, argrot);
								break;
								case 1:
									shipload[i][j] = new Batterie(pm,sb.es.ship, argpart,argmaterial,i,j, argrot);
									break;
								case 2:
									shipload[i][j] = new Dreieck(pm,sb.es.ship, argpart,argmaterial,i,j, argrot);
									break;
								case 3:
									shipload[i][j] = new Generator(pm,sb.es.ship, argpart,argmaterial,i,j, argrot);
									break;
								case 4:
									shipload[i][j] = new Kern(pm,sb.es.ship, argpart,argmaterial,i,j, argrot);
									break;
								case 5:
									shipload[i][j] = new Panzerung(pm,sb.es.ship, argpart,argmaterial,i,j, argrot);
									break;
								case 6:
									shipload[i][j] = new Quartiere(pm,sb.es.ship, argpart,argmaterial,i,j, argrot);
									break;
								case 7:
									shipload[i][j] = new Schildgenerator(pm,sb.es.ship, argpart,argmaterial,i,j, argrot);
									break;
								case 8:
									shipload[i][j] = new Spitze(pm,sb.es.ship, argpart,argmaterial,i,j, argrot);
									break;
								case 9:
									shipload[i][j] = new Steuerung(pm,sb.es.ship, argpart,argmaterial,i,j, argrot);
									break;
								case 10:
									shipload[i][j] = new Turret(pm,sb.es.ship, argpart,argmaterial,i,j, argrot);
									break;
								
								
								
								}
							}
						}
					}
				}
			}
			return shipload;
			
		}
	  
	  
	  /**
  	 *Das Auslesen einer Schiffsdatei
  	 *
  	 * @param fh Die Datei des Schiffs
  	 * @return die Daten des Schiffs als String
  	 */
  	String readship(FileHandle fh) {
		  shiphandle = fh;
		  String data = shiphandle.readString();
		  return data;
		  
	  }
	  
}