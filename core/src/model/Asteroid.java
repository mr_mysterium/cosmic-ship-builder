package model;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.hgs.ShipBuilder;

import net.dermetfan.gdx.graphics.g2d.Box2DSprite;
import part.AsteroidPart;


/**
 * Die Klasse Asteroid ist zust�ndig f�r die Generierung sowie die physikalische Darstellung aller Asteroiden
 */
public class Asteroid {

/** Zugriff auf die Hauptinstanz des spiels */
ShipBuilder game;

/** Zugriff auf die physikalische Welt von Box2d */
World w;

/** Der physikalische K�rper der aus kleineren K�rpern , den Fixtures zusammengesetzt ist */
Body body;

/** Die Definition des physikalischen K�rpers */
BodyDef bd;

/** Die Definition der Teilk�rper */
FixtureDef fixtureDef;

/** Das Array das die logischen Bestandteile des Asteroids enth�lt */
AsteroidPart[][] asteroidmodel;

/** Position des Asteroids die zuf�llig gesetzt wird */
Vector2 position;

/** Das Material eines Asteroiden kann wie bei Schiffen zwischen 1-4 Varieren und steht dabei f�r die Materialien : Chromium,Cosmium,Emeraldium,Infernium */
int material;

/** Ein gr��enfaktor der die gr��e des Asteroiden mitbestimmt */
int size;

/** Die gr��e des Asteroiden kann entweder 3x3 oder 5x5 Felder betragen*/
int gridsize;

/** Ein Skalarfaktor mit dem man die gr��e des fertigen asteroiden bestimmen kann */
int scale;

/** Wird f�r die zuf�llige erstellung von Zahlen verwendet*/
Random randomizer;

/** Der physikalische Teilk�rper der zusammengestzt, zu einem Body wird */
Fixture fixture;

/** Eine Hilfsklasse aus der libGDX Utils library die das zeichnen von Bildern auf Fixtures vereinfacht */
Box2DSprite box2dSprite;

/** Die Definition eines dreieckigen Teilk�rpers*/
FixtureDef trifixtureDef;

/** Die Definition eines spitzen Teilk�rpers */
FixtureDef tipfixtureDef;

/** Die Definition eines boxf�rmigen Boxk�rpers */
FixtureDef boxfixtureDef;

/** Eine Polygonform die aus mehreren 2d Ortsvektoren eine konvexe Dreiecksform bildet die f�r den Teilk�rer verwendet wird */
PolygonShape trishape;

/** Eine Polygonform die aus mehreren 2d Ortsvektoren eine konvexe Spitzenform bildet die f�r den Teilk�rer verwendet wird */
PolygonShape tipshape;

/** Eine Polygonform die aus mehreren 2d Ortsvektoren eine konvexe Boxform bildet die f�r den Teilk�rer verwendet wird  */
PolygonShape boxshape;

/** Ein Array aus Vektoren das zur Dreiecksform wird */
Vector2[] trivert;

/** Ein Array aus Vektoren das zur Spitzenform wird*/
Vector2[] tipvert;

/** Ein Array aus Vektoren das zur Boxform wird */
Vector2[] boxvert;

/**
 * Der Konstruktor des Asteroiden initialisiert erst die Definitionen der Teilk�rper,die Polygonformen und den physikalischen Asteroidenk�rper
 * bevor er sich durch einen Algorithmus selbst generiert 
 * und zum Schluss den Tag "Asteroid" bekommt damit er sp�ter bei der Kollisionsberechnung identifiziert werden kann
 *
 * @param game die Hauptinstanz des Programms
 * @param w Die physikalische Welt in der der Asteroid platziert werden soll
 */
public Asteroid(ShipBuilder game, World w) {
	this.game = game;
	this.w = w;
	randomizer = new Random();
	size = randomizer.nextInt(2);
	scale =2+2*size;
	material = 1+ randomizer.nextInt(4);
	
	trifixtureDef = new FixtureDef();
	trifixtureDef.density =1;
	trifixtureDef.filter.categoryBits = Box2dFILTER.BIT_ASTERODIPART;
	trifixtureDef.filter.maskBits = Box2dFILTER.BIT_PART|Box2dFILTER.BIT_ASTERODIPART|Box2dFILTER.BIT_LASER;
    tipfixtureDef = new FixtureDef();
    tipfixtureDef.density = 1;
    tipfixtureDef.filter.categoryBits = Box2dFILTER.BIT_ASTERODIPART;
	tipfixtureDef.filter.maskBits = Box2dFILTER.BIT_PART|Box2dFILTER.BIT_ASTERODIPART|Box2dFILTER.BIT_LASER;;
	boxfixtureDef = new FixtureDef();
	boxfixtureDef.density=1;
	boxfixtureDef.filter.categoryBits = Box2dFILTER.BIT_ASTERODIPART;
	boxfixtureDef.filter.maskBits = Box2dFILTER.BIT_PART|Box2dFILTER.BIT_ASTERODIPART|Box2dFILTER.BIT_LASER;;
	
	trishape = new PolygonShape();
	tipshape = new PolygonShape();
	boxshape = new PolygonShape();
	
	trivert = new Vector2[3];
	tipvert = new Vector2[3];
	boxvert = new Vector2[4];
	
	bd = new BodyDef();
	bd.type = BodyType.DynamicBody;
	bd.angularDamping= 0.8f;
	body = w.createBody(bd);
	body.setLinearDamping(0.76f);
	body.setTransform(new Vector2((randomizer.nextInt(ShipBuilder.WIDTH*9/5)), (randomizer.nextInt(ShipBuilder.HEIGHT*16/5))),randomizer.nextInt(360));
	generatefixtures(size, material);
	body.setUserData("asteroid");
}

/**
 * Generierung des physikalischen Asteroidenk�rpers.
 * Asteroiden der gr��e 0 werden aus einem Set von vorgegeben Asteroiden ausgew�hlt 
 * Asteroiden der gr��e 1 werden durch einen Algorithmus zuf�llig generiert
 *
 * @param size Die gr��e des Asteroiden
 * @param material Das Material aus dem der Asteroid bestehen soll
 * @return the body Der nun generierte physikalische k�rper wird zur�ckgegeben
 */
private Body generatefixtures(int size,int material) {
	Body asteroidbody = null;
	switch (size) {
	case 0:
		asteroidmodel = new AsteroidPart[3][3];
		int shape = randomizer.nextInt(3);
		 for (int i = 0; i < asteroidmodel.length; i++) {
				for (int j = 0; j <asteroidmodel[i].length; j++) {
					chooseshape(shape, i, j);
				}
			} 
		break;
	case 1:
		asteroidmodel = new AsteroidPart[5][5];
		TEMPLATE.createcomposite(size, material);
		 for (int i = 0; i < asteroidmodel.length; i++) {
				for (int j = 0; j <asteroidmodel[i].length; j++) {
					addmodule(TEMPLATE.addcomposite(size, material,i,j), i, j);
				}
			} 
		break;
	}
	return asteroidbody;
}

/**
 * Schrittweise Erzeugung des vordefinierten Asteroiden
 *
 * @param shape Die per zufall ausgew�hlte Form des kleinen Asteroiden
 * @param i the Teil der gerade auszuf�llenden position
 * @param j the Teil der gerade auszuf�llenden position
 */
private void chooseshape(int shape, int i, int j) {
	switch (shape) {
	case 0:
		addmodule(TEMPLATE.addround(3, material, i, j),i,j);
		break;
	case 1:
		addmodule(TEMPLATE.adddiagonal(3, material, i, j),i,j);
		break;
	case 2:
		addmodule(TEMPLATE.addspikey(3, material, i, j),i,j);
		break;

	default:
		break;
	}
	
}

/**
 * Schrittweises Erzeugung des generierten Asteroiden
 *
 * @param asteroidPart das zu hinzuf�gende Teil des Asteroiden
 * @param gridposx Teil der gerade auszuf�llenden position
 * @param gridposy Teil der gerade auszuf�llenden position
 */
private void addmodule(AsteroidPart asteroidPart, int gridposx, int gridposy) {
	if (asteroidPart!=null) {
		box2dSprite= new Box2DSprite(asteroidPart.getSprite());
		int x = gridposx-gridsize/2+1;
		int y = gridposy-gridsize/2+1;
		if (asteroidPart.getPart().equals("steinkante")) {
			trivert[0] = new Vector2(x*scale,y*scale);
			trivert[1] = new Vector2(x*scale+scale,y*scale);
			trivert[2] = new Vector2(x*scale,y*scale+scale);
			float originx = trivert[0].x+scale/2f;
			float originy = trivert[0].y+scale/2f;
		float angle =(float) ((90+asteroidPart.getRotation())); 
			for (int i = 0; i < trivert.length; i++) {
				trivert[i] = UTILITY.rotatevec(trivert[i], angle,originx,originy);
			
			}
			
			trishape.set(trivert);
			trifixtureDef.shape = trishape;
			fixture =body.createFixture(trifixtureDef);
			box2dSprite.setRotation(angle);
			fixture.setUserData(box2dSprite);
		} else if (asteroidPart.getPart().equals("steinspitze")) {
			tipvert[0] = new Vector2(x*scale,y*scale);
			tipvert[1] = new Vector2(x*scale+scale, y*scale);
			tipvert[2] = new Vector2(x*scale+scale/2f, y*scale+scale);
			float angle =(float) ((90+asteroidPart.getRotation())); 
			float originx = tipvert[0].x+scale/2f;
			float originy = tipvert[0].y+scale/2f;
			for (int i = 0; i < tipvert.length; i++) {
				tipvert[i] = UTILITY.rotatevec(tipvert[i], angle,originx,originy);
			
			}
			tipshape.set(tipvert);
			tipfixtureDef.shape = tipshape;
			fixture = body.createFixture(tipfixtureDef);
		    box2dSprite.setRotation(angle);
			fixture.setUserData(box2dSprite);
			
		} else {
			boxvert[0] = new Vector2(x*scale,y*scale);
			boxvert[1] = new Vector2(x*scale+scale, y*scale);
			boxvert[2] = new Vector2(x*scale+scale, y*scale+scale);
			boxvert[3] = new Vector2(x*scale, y*scale+scale);
			float angle =(float) ((90-asteroidPart.getRotation())); 
			float originx = boxvert[0].x+scale/2f;
			float originy = boxvert[0].y+scale/2f;
			for (int i = 0; i < boxvert.length; i++) {
				boxvert[i] = UTILITY.rotatevec(boxvert[i], angle,originx,originy);
			
			}
			boxshape.set(boxvert);
			boxfixtureDef.shape = boxshape;
			fixture = body.createFixture(boxfixtureDef);
			box2dSprite.setRotation(angle);
			fixture.setUserData(box2dSprite);
		}
	}
	}



/**
 * Gibt den physikalischen K�rper zur�ck
 *
 * @return der physikalische K�rper
 */
public Body getBody() {
	return body;
}
	

}
