package Scenes;

import java.text.DecimalFormat;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.hgs.ShipBuilder;

import Screens.EditorScreen;
import model.Confirmlistener;
import model.Ship;

public class ShipInfo {
	ShipBuilder sb;
	public Stage stage;
	Table table;
public static BitmapFont statfont= new BitmapFont(Gdx.files.internal("statfont.fnt"),Gdx.files.internal("statfont.png"),false);
	LabelStyle ls = new LabelStyle(statfont, Color.SKY);
    Image shipinfo = new Image(new Texture("infopanel.png"));
	float width= ShipBuilder.WIDTH*0.225f;
	float height=  ShipBuilder.HEIGHT*0.85f;
	float posx= ShipBuilder.WIDTH-width;
	float posy= (ShipBuilder.HEIGHT*0.9f-height)/2;
	Label name= new Label("Name: Unnamed",ls);
	Label parts= new Label("Parts: " +"0/0",ls);;
	Label hp= new Label("Hull: 0 HP",ls);;
	Label shield= new Label("Shield: 0 SP",ls);;
	Label mass= new Label("Mass: 0 T",ls);;
	Label accelaration= new Label("Accelaration: 0 KM/S^2",ls);;
	Label maxspeed= new Label("Speedlimit: 0 KM/S",ls);;
	Label anglvel= new Label("Angular Velocity: 0 r/s",ls);;
    Label breakvel= new Label("Decelaration: 0 KM/S^2",ls);
    Label energyproduction= new Label("Energy Supply 0 KW: ",ls);;
	Label battery= new Label("Battery: 0 KW/S",ls);;
	Label energyconsumption= new Label("Energy Usage: 0 KW",ls);;
	Label crew= new Label("Crew: 0/0",ls);
	
	
	public ShipInfo(final ShipBuilder sb,final EditorScreen es) {
		this.sb = sb;
		statfont.getData().setScale(0.85f);
		final Skin uiSkin = new Skin(Gdx.files.internal("uiskin.json"));
		Dialog dialog = new Dialog("", uiSkin, "dialog") {
		    public void result(Object obj) {
		        if (obj.equals(true) ) {
		        	Window nameinput = new Window("", uiSkin);
		        	nameinput.left();
		        	nameinput.setMovable(false);
		        	nameinput.setResizable(false);
		        	nameinput.setWidth(ShipBuilder.WIDTH/2);
		    		nameinput.setPosition(ShipBuilder.WIDTH/2-nameinput.getWidth()/2, ShipBuilder.HEIGHT/2-nameinput.getHeight()/2);
		    		Label guide = new Label("Shipname: ", uiSkin);
		    		TextField tf = new TextField("", uiSkin);
		    		tf.setMaxLength(28);
		    		tf.setTextFieldFilter(new TextFieldFilter() {
		    		    public  boolean acceptChar(TextField textField, char c) {
		    		    	String s = String.valueOf(c);
		    		         if (s.matches("^[a-zA-Z0-9]*$"))
		    		               return true;
		    		         return false;
		    		    }
		    		});
		    		TextButton confirm = new TextButton("confirm",uiSkin);
		    		confirm.addListener(new Confirmlistener(nameinput,tf, sb));
		    		nameinput.setHeight(ShipBuilder.HEIGHT/12);
		        	nameinput.add(guide).left();
		        	nameinput.add(tf).padLeft(nameinput.getWidth()/20).width(nameinput.getWidth()*0.5f);
		        	nameinput.add(confirm).padLeft(nameinput.getWidth()/20).size(nameinput.getWidth()/5f,nameinput.getHeight()/2);
		        	stage.addActor(nameinput);
		        	
					
				}else {
					es.storage.update(es.browser.shiplist);
					stage.addActor(es.browser);
					es.browser.setVisible(true);
					es.setIndialog(true);
				}
		        this.remove();
		        
		    }
		};
		dialog.setMovable(false);
		dialog.getButtonTable().defaults().size(ShipBuilder.WIDTH/10, ShipBuilder.HEIGHT/15);
		dialog.text("Do you want to load an existing ship");
		dialog.getContentTable().row();
		dialog.text("or create a new one?");
		dialog.button("Load", false); //sends "true" as the result
		dialog.button("Create New", true); //sends "false" as the result
		dialog.setSize(ShipBuilder.WIDTH/3, ShipBuilder.HEIGHT/4);
		dialog.setPosition(ShipBuilder.WIDTH/2-dialog.getWidth()/2, ShipBuilder.HEIGHT/2-dialog.getHeight()/2);

		statfont.getData().markupEnabled = true;
		stage = new Stage();
		shipinfo.setSize(width, height);
		shipinfo.setPosition(posx,posy);
	    table = new Table();
	    table.left();
		table.setSize(width,height);
		table.setPosition(posx,posy);
		table.add(name).expandY().left().padLeft(width/15);
		table.row();
		table.add(parts).expandY().left().padLeft(width/15);
		table.row();
		table.add(hp).expandY().left().padLeft(width/15);
		table.row();
		table.add(shield).expandY().left().padLeft(width/15);
		table.row();
		table.add(mass).expandY().left().padLeft(width/15);
		table.row();
		table.add(accelaration).expandY().left().padLeft(width/15);
		table.row();
		table.add(maxspeed).expandY().left().padLeft(width/15);
		table.row();
		table.add(anglvel).expandY().left().padLeft(width/15);
		table.row();
		table.add(breakvel).expandY().left().padLeft(width/15);
		table.row();
		table.add(energyproduction).left().expandY().padLeft(width/15);
		table.row();
		table.add(battery).expandY().left().padLeft(width/15);
		table.row();
		table.add(energyconsumption).left().expandY().padLeft(width/15);
		table.row();
		table.add(crew).expandY().left().padLeft(width/15);
		stage.addActor(shipinfo);
		table.row();
		stage.addActor(table);
		stage.addActor(dialog);
	
		
		
		
	}
	/**
	 * @param s
	 */
	public void updateinfo(Ship s) {
	    name.setText("Name: "+s.getName());
	    parts.setText("Parts: "+s.getParts()+"/"+s.getPartsmax());
	    hp.setText("Hull: " +s.getHp()+ " HP");
	    mass.setText("Mass: " +s.getMass() +" T");
	    energyconsumption.setText("Energy Usage: "+s.getEnergyconsumption()+" KW");
	    accelaration.setText("Accelaration: "+new DecimalFormat("###.##").format(s.getAccelaration())+" KM/S^2");
	    battery.setText("Battery: "+s.getBattery()+ " KW/S");
	    energyproduction.setText("Energy Supply "+s.getEnergyproduction()+" KW: ");
		crew.setText("Crew: "+s.getCrew()+"/"+s.getCrewneed());
		shield.setText("Shield: "+s.getShield()+ " SP");
		maxspeed.setText("Speedlimit: "+new DecimalFormat("###.##").format(s.getMaxspeed())+" KM/S");
		breakvel.setText("Decelaration: "+new DecimalFormat("###.##").format(s.getDecelaration())+" KM/S^2");
		anglvel.setText("Angular Velocity: "+new DecimalFormat("###.##").format(s.getRotationspeed())+" r/s");
		
	}
	
	

		
	
}
