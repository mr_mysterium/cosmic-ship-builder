package Screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.hgs.ShipBuilder;

import Scenes.PartMenu;
import Scenes.ShipInfo;
import Scenes.Shipbrowser;
import model.ConnectedAlg;
import model.Ship;
import model.ShipStorage;
import part.Antrieb;
import part.Batterie;
import part.Dreieck;
import part.Generator;
import part.Kern;
import part.Panzerung;
import part.Part;
import part.Quartiere;
import part.Schildgenerator;
import part.Spitze;
import part.Steuerung;
import part.Turret;

public class EditorScreen implements Screen, InputProcessor {
	Stage s;
	private ShipBuilder game;
	public ShipStorage storage;
	boolean indialog=false;
	OrthographicCamera camera;
	Texture grid;
	PartMenu pm;
	public Shipbrowser browser;
	Button browserbtn;
	ButtonStyle browserbtnstyle;
	Skin browserskin;
	ShipInfo si;
    InputMultiplexer multiplexer;
	Vector3 worldCoordinates;
	Float Partposx;
	Float Partposy;
	float maxzoom = 2.0f;
	float minzoom = 0.2f;
	float rotation = 0;
	float worldposx;
	float worldposy;
	int gridx;
	int gridy;
	Part[][] parts = new Part[32][32];
	int[][]partgraph= new int[32][32];
	public Ship ship;
	public EditorScreen(ShipBuilder game) {
		multiplexer= new InputMultiplexer();
		this.game = game;
		s= new Stage();
		storage= new ShipStorage(game);
		browser = new Shipbrowser(game,"Ship Browser",ShipBuilder.gameskin );
		browserskin = new Skin(new TextureAtlas("save.pack"));
		browserbtnstyle = new ButtonStyle();
		browserbtnstyle.up = browserskin.getDrawable("save_up");
	    browserbtnstyle.down = browserskin.getDrawable("save_down");
	    browserbtn= new Button(browserbtnstyle);
	    browserbtn.setBounds(0,0, ShipBuilder.HEIGHT/10, ShipBuilder.HEIGHT/10);
	    s.addActor(browserbtn);
	    s.addActor(browser);
	    browser.setVisible(false);
		camera = new OrthographicCamera(ShipBuilder.WIDTH / 2, ShipBuilder.HEIGHT / 2);
		grid = new Texture("grid.png");
		camera.translate(new Vector2(ShipBuilder.HEIGHT / 2, ShipBuilder.HEIGHT / 2));
		camera.zoom = 1f;
		pm = new PartMenu(game);
		si = new ShipInfo(game,this);
		multiplexer.addProcessor(s);
		multiplexer.addProcessor(this);
		multiplexer.addProcessor(pm.stage);
		multiplexer.addProcessor(si.stage);
		Gdx.input.setInputProcessor(multiplexer);

	}

	@Override
	public void show() {
		
		 browserbtn.addListener(new ChangeListener() {
		        

				@Override
				public void changed(ChangeEvent event, Actor actor) {
					if (ship!=null) {
						browser.setVisible(!browser.isVisible());
					    setIndialog(!indialog);
					}
					
				}
	        });
		
	}

	@Override
	public void render(float delta) {
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		worldCoordinates = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
		camera.unproject(worldCoordinates);
		worldCoordinates.x-=3;
		worldCoordinates.y-=10;
		worldposx = (int) (worldCoordinates.x / (ShipBuilder.HEIGHT / 47.25)) * ShipBuilder.HEIGHT / 47.25f + ShipBuilder.HEIGHT / 283.5f;
		worldposy = (int) (worldCoordinates.y / (ShipBuilder.HEIGHT / 47.25)) * ShipBuilder.HEIGHT / 47.25f + ShipBuilder.HEIGHT / 113.4f;
		gridx = (int) (worldCoordinates.x / (ShipBuilder.HEIGHT / 47.25)) - 11;
		gridy = -((int) (worldCoordinates.y / (ShipBuilder.HEIGHT / 47.25)) - 35);

		game.batch.begin();
		Gdx.gl.glClearColor(66 / 255f, 66 / 255f, 66 / 255f, 100);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.batch.draw(grid, 0, 0, ShipBuilder.HEIGHT, ShipBuilder.HEIGHT);
		for (int i = 0; i < parts.length; i++) {
			for (int j = 0; j < parts[i].length; j++) {
				if (parts[i][j] != null) {
					parts[i][j].getSprite().draw(game.batch);
				}
			}
		}
		if (pm.isPartSelected()) {

			Partposx = worldCoordinates.x;
			Partposy = worldCoordinates.y;
			TextureRegion currenttexture = new TextureRegion(ShipBuilder.gameskin.getRegion(pm.getSelectedpart()));
			Sprite spriteselect = new Sprite(currenttexture);
			spriteselect.setPosition(worldposx, worldposy);
			spriteselect.setRotation(rotation);
			if (ship!=null) {
				if (checklogic()&ship.getParts()<ship.getPartsmax()) {
					spriteselect.setColor(Color.GREEN);
				}else {
					spriteselect.setColor(Color.RED);
				}
				spriteselect.draw(game.batch);	
			}
		}
		game.batch.end();
		s.draw();
		pm.stage.draw();
		si.stage.draw();
		
		if (Gdx.input.isKeyPressed(Keys.FORWARD_DEL)) {
				pm.setPartSelected(false);
				if (checkgrid()) {
					if (parts[gridx][gridy] != null) {
						deletepart();	
					}
					
				}
		}
		
	}

	

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void dispose() {
		 
	}

	@Override
	
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public boolean keyDown(int keycode) {
		
		if (Gdx.input.isKeyPressed(Keys.R)) {	
			if (rotation<270) {
				rotation += 90;
				storage.update(browser.shiplist);
			}else {
				rotation=0;
			}
		}
		
			if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
				if (indialog&ship!=null) {
					indialog = false;
					browser.setVisible(false);
				} else {
					if (!pm.isPartSelected()) {
						dispose();
						game.setScreen(new MenuScreen(game));
						
					}
					pm.setPartSelected(false);
				}
				
				
			
			
			
			
			
		}
		return false;
}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (!indialog) {
			if (Gdx.input.getY()>ShipBuilder.HEIGHT/10) {
				if (checklogic()) {
					placepart();	
				}
				
			}
		}
		
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		if (!indialog) {
			if (Gdx.input.isButtonPressed(Buttons.LEFT)) {
				if (ship!=null) {
					if (Gdx.input.getY()>ShipBuilder.HEIGHT/10) {
						if (checklogic()) {
							placepart();
						}
					}
				}
			
					
				
			}
			if (Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
				
				if (ship!=null) {
					float x = Gdx.input.getDeltaX();
					float y = Gdx.input.getDeltaY();
					if (y > 0) {
						if (camera.position.y < ShipBuilder.HEIGHT - (ShipBuilder.HEIGHT * camera.zoom / maxzoom / 2)) {
							camera.translate(0, y * (camera.zoom / maxzoom));
						}
					} else {
						if (camera.position.y > (ShipBuilder.HEIGHT * camera.zoom / maxzoom / 2)) {
							camera.translate(0, y * (camera.zoom / maxzoom));
						}
					}

					if (x < 0) {
						if (camera.position.x < ShipBuilder.HEIGHT - (ShipBuilder.HEIGHT * camera.zoom /maxzoom / 2)) {
							camera.translate(-x * (camera.zoom / maxzoom), 0);
						}
					} else {
						if (camera.position.x > (ShipBuilder.HEIGHT / maxzoom / 2)) {
							camera.translate(-x * (camera.zoom /maxzoom), 0);
						}
					}
				}
				
			
				
			}
		}
		
		
		return false;
	}

	public boolean isIndialog() {
		return indialog;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		if (!indialog) {
			if (ship!=null) {
				if (amount > 0) {
					if (camera.zoom < maxzoom) {
						camera.zoom += amount * 0.1;
						

					}
				} else {
					if (camera.zoom > minzoom) {
						camera.zoom += amount * 0.1;
						
					}
				}

				
			}	
		}
		return false;		
	}
	
	public boolean checklogic() {
		if (ship!=null) {
			if (checkgrid()&&checkcore()&&checkadjacent()&&checklimit()) {
				return true;
			}else {
				return false;
			}	
		} else {
			return false;
		}
		
	}
	
	private boolean checklimit() {
		if (checkgrid()) {
			if (ship.getParts()<ship.getPartsmax()||parts[gridx][gridy]!=null) {
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}
		
		
	}

	public boolean checkgrid(){
		if (gridx >= 0 && gridy >= 0 && gridx <= 31 && gridy <= 31) {
			return true;
		}else {
			return false;
		}
	}
	public boolean checkcore(){
		boolean kern= false;
		for (int i = 0; i < parts.length; i++) {
			for (int j = 0; j < parts[i].length; j++) {
				if (parts[j][i] != null) {
				if (parts[j][i].getPart().contains("kern")) {
					kern = true;
					}
				}
			}
		}
	
		
		if ( (parts[gridx][gridy]!= null &&pm.getSelectedpart()!=null) && parts[gridx][gridy].getPart().contains("kern")&&pm.getSelectedpart().contains("kern")&&
			ship.getParts()<=new Kern(pm, ship, pm.getSelectedpart().substring(0,pm.getSelectedpart().length()-2), Integer.parseInt(pm.getSelectedpart().substring(pm.getSelectedpart().length()-1)),0,0,0).getPartsmax()) {
			return true;
		}
		
		if (pm.getSelectedpart()!=null) {
			if (pm.getSelectedpart().contains("kern")&&kern==false) {
				return true;
			}else if (parts[gridx][gridy]!=null) {
				if (kern==true&&pm.getSelectedpart().contains("kern")==false&&!parts[gridx][gridy].getPart().contains("kern")) {
					return true;
				}
			}
			else if(kern==true&&pm.getSelectedpart().contains("kern")==false) {
				return true;
			}
			
		}
		return false;
	}
	
	public boolean checkadjacent() {
		
		boolean kern = false;
		for (int i = 0; i < parts.length; i++) {
			for (int j = 0; j < parts[i].length; j++) {
				if (parts[j][i] != null) {
				if (parts[j][i].getPart().contains("kern")) {
					kern = true;
					}
				}
			}
		}
		if (kern == false) {
			return true;
		}
		if (gridx >= 1) {
			if (parts[gridx - 1][gridy] != null) {
				return true;
			}
		}
		if (gridx <= 30) {
			if (parts[gridx + 1][gridy] != null) {
				return true;
			}
		}
		if (gridy >= 1) {
			if (parts[gridx][gridy - 1] != null) {
				return true;
			}
		}
		if (gridy <= 30) {
			if (parts[gridx][gridy + 1] != null) {
				return true;
			}
		}
		if (parts[gridx][gridy]!=null) {
			return true;
		}
		return false;
	}
	

	public boolean checkconnectivity() {
		partgraph[gridx][gridy]=0;
		ConnectedAlg checkcon = new ConnectedAlg();
	
		if (checkcon.findIslandsCount(partgraph)==1) {
			updatepartgraph();
			return true;
		}else {
			updatepartgraph();
			return false;
		}
		
	}
	
	public void placepart() {
		if (Gdx.input.isButtonPressed(Buttons.LEFT)) {

			if (pm.isPartSelected()) {
					
					String part = pm.getSelectedpart().substring(0,pm.getSelectedpart().length()-2);
					int material = Integer.parseInt(pm.getSelectedpart().substring(pm.getSelectedpart().length()-1));
					
					for (int i = 0; i<=10; i++) {
						if (part.equals(pm.parttypes[i])) {
							switch (i) {
							case 0:
								parts[gridx][gridy] = new Antrieb(pm,ship, part,material,gridx,gridy, rotation);
							break;
							case 1:
								parts[gridx][gridy] = new Batterie(pm,ship, part,material,gridx,gridy, rotation);
								break;
							case 2:
								parts[gridx][gridy] = new Dreieck(pm,ship, part,material,gridx,gridy, rotation);
								break;
							case 3:
								parts[gridx][gridy] = new Generator(pm,ship, part,material,gridx,gridy, rotation);
								break;
							case 4:
								parts[gridx][gridy] = new Kern(pm,ship, part,material,gridx,gridy, rotation);
								break;
							case 5:
								parts[gridx][gridy] = new Panzerung(pm,ship, part,material,gridx,gridy, rotation);
								break;
							case 6:
								parts[gridx][gridy] = new Quartiere(pm,ship, part,material,gridx,gridy, rotation);
								break;
							case 7:
								parts[gridx][gridy] = new Schildgenerator(pm,ship, part,material,gridx,gridy, rotation);
								break;
							case 8:
								parts[gridx][gridy] = new Spitze(pm,ship, part,material,gridx,gridy, rotation);
								break;
							case 9:
								parts[gridx][gridy] = new Steuerung(pm,ship, part,material,gridx,gridy, rotation);
								break;
							case 10:
								parts[gridx][gridy] = new Turret(pm,ship, part,material,gridx,gridy, rotation);
								break;
							
							
							
							
							}
						}
					}
					
					
				
					updatepartgraph();
					storage.saveship();
					ship.update(parts);
					si.updateinfo(ship);
					
					
				

			}
		}
	}	
	public void deletepart() {
	
		
			if (parts[gridx][gridy].getPart().contains("kern") && countparts() == 1) {
				parts[gridx][gridy] = null;
			} else if (checkconnectivity() && !parts[gridx][gridy].getPart().contains("kern")) {
				parts[gridx][gridy] = null;
		}
			storage.saveship();
			updatepartgraph();
			ship.update(parts);
			si.updateinfo(ship);
			
		
	}
		public void updatepartgraph(){
			for (int i = 0; i < parts.length; i++) {
				for (int j = 0; j < parts[i].length; j++) {
					if (parts[j][i] != null) {
						partgraph[j][i]=1;
					}else {
						partgraph[j][i]=0;
					}
				}
			}
		}
	public int countparts() {
	int partcount=0;
	for (int i = 0; i < parts.length; i++) {
		for (int j = 0; j < parts[i].length; j++) {
			if (partgraph[j][i]==1) {
				partcount++;
			}		
		}
	}
		return partcount;
		
	}
	public void createship(String name) {
		ship = new Ship(name);
		si.updateinfo(ship);
	}

	public void setIndialog(boolean indialog) {
		this.indialog = indialog;
	}

	public ShipStorage getStorage() {
		return storage;
	}

	public void setStorage(ShipStorage storage) {
		this.storage = storage;
	}

	public Part[][] getParts() {
		return parts;
	}

	public void setParts(Part[][] parts) {
		this.parts = parts;
	}

	public PartMenu getPm() {
		return pm;
	}

	public ShipInfo getSi() {
		return si;
	}
	
	
}
