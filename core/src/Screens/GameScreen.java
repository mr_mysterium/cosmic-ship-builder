package Screens;

import java.awt.RenderingHints.Key;
import java.util.ArrayList;

import javax.jws.soap.SOAPBinding.Use;

import org.omg.CORBA.PRIVATE_MEMBER;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.particles.influencers.ColorInfluencer.Random;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2D;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Box2DUtils;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.QueryCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.hgs.ShipBuilder;

import Scenes.Shipselect;
import model.Asteroid;
import model.Blueprint;
import model.CollisionDetector;
import model.ConnectedAlg;
import model.Laser;
import model.Player;
import model.ScrollingBackground;
import model.ShipStorage;
import model.UserData;
import net.dermetfan.gdx.graphics.g2d.Box2DSprite;

public class GameScreen implements Screen,InputProcessor {
	private ShipBuilder game;
	float maxzoom = 2f;
	float minzoom = 0.01f;
    Player player;
	World world;
	ArrayList<Array<Vector2>> toremove;
	ConnectedAlg con;
	ContactListener detector;
	ArrayList<Fixture> todestroy;
	ArrayList<Laser>todeactivate;
	MouseJointDef jointDef;
	MouseJoint joint;
	InputMultiplexer multiplexer;
	Box2DDebugRenderer debugRenderer;
	Stage s;
	Shipselect select;
	OrthographicCamera cam;
	ScrollingBackground bg;
	ArrayList<Vector2>turretpositions= new ArrayList<Vector2>();
	ArrayList<Fixture> tomine= new ArrayList<Fixture>();
	 public GameScreen(ShipBuilder game) {
		    this.game = game;	
		    multiplexer = new InputMultiplexer();
		    Gdx.gl.glClearColor(0,0,0, 100);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		    cam = new OrthographicCamera(ShipBuilder.WIDTH,ShipBuilder.HEIGHT);
		    cam.position.set(ShipBuilder.WIDTH/2,ShipBuilder.HEIGHT/2,0);
		    cam.zoom = minzoom*10;
	        world = new World(new Vector2(0, 0), true ); 
	        con = new ConnectedAlg();
	        todestroy = new ArrayList<Fixture>();
	        debugRenderer = new Box2DDebugRenderer();
	        s= new Stage();
	        select = new Shipselect("", ShipBuilder.gameskin, game);
	        s.addActor(select);
	        multiplexer.addProcessor(this);
	        multiplexer.addProcessor(s);
	        Gdx.input.setInputProcessor(multiplexer);
	        
	        
	 }
	
	@Override
	public void show() {		
		}
		
	

	@Override
	public void render(float delta) {
		if (player!=null) {
			update(Gdx.graphics.getDeltaTime());
		}
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        if (bg!=null) {
          bg.draw(game.batch);
		}
        s.draw();
        game.batch.begin();	
        game.batch.setProjectionMatrix(cam.combined); 
        Box2DSprite.draw(game.batch, world);
        
        for (int i = ShipBuilder.effects.particlelist.size()-1; i >0 ; i--) {
    		if (ShipBuilder.effects.particlelist.get(i)!=null) {
    			ShipBuilder.effects.particlelist.get(i).draw(game.batch);
    		}
    	}
        game.batch.end();
        //debugRenderer.render(world, cam.combined);
        
  
   		
	}
	
	

	private void update(float deltaTime) {
		Gdx.graphics.setTitle("FPS:"+Gdx.graphics.getFramesPerSecond());
		focusplayer(deltaTime);
		cam.update();
		manageinput(deltaTime);
		manageFX(deltaTime);
		try {
			world.step(deltaTime, 10, 10);
		    } catch (Exception e) {}
		managelasers();
		managedestruction();
		
		
	}
	

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		debugRenderer.dispose();
		world.dispose();
		
	}

	@Override
	public boolean keyDown(int keycode) {
		if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
			game.setScreen(new MenuScreen(game));
			}
		
		if (Gdx.input.isKeyPressed(Keys.ENTER)) {
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		
		
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
	
		return false;
	}
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		 if (player!=null) {
  		   Laser.active = true;
		 }
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		Laser.active =false;

		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		if (amount > 0) {
			if (cam.zoom < maxzoom) {
				cam.zoom *= 1.05f;
				

			}
		} else {
			if (cam.zoom > minzoom) {
				cam.zoom *= 0.95f;
				
			}
		}
	

		
	
	
		return false;
	}

	private void focusplayer(float deltatime) {
		if (player!=null) {
			Vector2 pos  = player.getblueprint().getBody().getWorldCenter(); 
	        Vector3 target = new Vector3(pos.x,pos.y,0); 
	        final float speed=deltatime*10,ispeed=1.0f-speed;
	        Vector3 cameraPosition = cam.position;
	        cameraPosition.scl(ispeed);
	        target.scl(speed);
	        cameraPosition.add(target);
	        cam.position.set(cameraPosition);
			
		}
	}
	
private void manageFX(float deltaTime) {
	for (int i = ShipBuilder.effects.particlelist.size()-1; i >0 ; i--) {
		if (ShipBuilder.effects.particlelist.get(i)!=null) {
			ShipBuilder.effects.particlelist.get(i).update(deltaTime);
			
		}
	}
	}
	
	private void manageinput(float deltaTime) {
			
			if (player!=null) {
			float acceleration=0;
			float angularspeed= (float) (player.getShip().getRotationspeed()*10);
			player.setThrustinput(0);
			player.setSteeringinput(0);
			if (Gdx.input.isKeyPressed(Keys.UP)&&!Gdx.input.isKeyPressed(Keys.DOWN)) {
				player.setThrustinput(-1);
				acceleration= (float) (1000*player.getShip().getAccelaration());
			}
	        if (Gdx.input.isKeyPressed(Keys.DOWN)&&!Gdx.input.isKeyPressed(Keys.UP)) {
				player.setThrustinput(1);
				acceleration= (float) (1000*player.getShip().getDecelaration());
			}
	        if (Gdx.input.isKeyPressed(Keys.LEFT)&&!Gdx.input.isKeyPressed(Keys.RIGHT)) {
				player.setSteeringinput(1);
			}
	        if (Gdx.input.isKeyPressed(Keys.RIGHT)&&!Gdx.input.isKeyPressed(Keys.LEFT)) {
	        	player.setSteeringinput(-1);
	        }
	        float angle = player.getblueprint().getBody().getAngle();
	        player.getblueprint().getBody().setAngularVelocity(angularspeed*player.getSteeringinput());
	        player.getblueprint().getBody().applyForceToCenter(-(float)Math.sin(angle)*acceleration*player.getThrustinput()*deltaTime*player.getblueprint().getBody().getMass(),
	    	        										    (float)Math.cos(angle)*acceleration*player.getThrustinput()*deltaTime*player.getblueprint().getBody().getMass(),true);
		}
		
	}
	
	private void managedestruction() {
		if (player!=null) {

			for (Fixture fixture : todestroy) {
				UserData data = (UserData) fixture.getUserData();
				player.getShip().getShipmodel()[(int) data.metadata.x][(int) data.metadata.y]=null;
				player.updatepartgraph();
				player.getblueprint().getBody().destroyFixture(fixture);
				
				    toremove=  con.findbrokenparts(player.getPartgraph(), player.getShip().getShipmodel());
				    player.updatepartgraph();
					Array<Fixture> fixtures= player.getblueprint().getBody().getFixtureList();
					for (int i = 0; i < toremove.size(); i++) {
						for (int j = 0; j <toremove.get(i).size; j++) {
							for (int k = 0; k < fixtures.size; k++) {
								UserData metadata = (UserData) fixtures.get(k).getUserData();
								if (metadata.metadata.x==toremove.get(i).get(j).x&&metadata.metadata.y==toremove.get(i).get(j).y) {
									player.getblueprint().getBody().destroyFixture(fixtures.get(k));
									
								}
							}
						
						}
					}
					toremove.clear();}
			if (!player.isAlive()) {
				game.gs = new GameScreen(game);
				game.setScreen(game.gs);
				
			}
			player.getShip().update(player.getShip().getShipmodel());
				}
		
			todestroy.clear();	
			
			for (Fixture fixture : tomine) {
				fixture.getBody().destroyFixture(fixture);
			}
			tomine.clear();
		
	}
	
	private void managelasers() {
		for (Laser laser : Laser.laserlist) {
			laser.getLaserbody().destroyFixture(laser.getLaserfixture());
			world.destroyBody(laser.getLaserbody());
		    }
		Laser.laserlist.clear();
		
		if (player!=null) {
			Vector3 mouse3d = new Vector3(Gdx.input.getX(), Gdx.input.getY(), Gdx.input.getY());
			cam.unproject(mouse3d);
			Vector2 mouseworldpos = new Vector2(mouse3d.x, mouse3d.y);
			
			Array<Fixture> fixtures = player.getblueprint().getBody().getFixtureList();
			for (Fixture fixture : fixtures) {
				try {
					UserData data = (UserData) fixture.getUserData();
					if (player.getShip().getShipmodel()[(int) data.metadata.x][(int) data.metadata.y].getPart().equals("turret")) {
						Rectangle boundingbox = net.dermetfan.gdx.physics.box2d.Box2DUtils.aabb(fixture);
						Vector2 center= new Vector2();
						boundingbox.getCenter(center);
						turretpositions.add(center);
						Laser laser = new Laser(world, player.getShip().getShipmodel()[(int) data.metadata.x][(int) data.metadata.y].getMaterial() );
						
						
					}
				} catch (Exception e) {}
			}
			if (Laser.active) {
				for (int i=0;i<Laser.laserlist.size();i++) {
					Laser.laserlist.get(i).update(turretpositions.get(i), player.getblueprint().getBody().getAngle(),mouseworldpos);
				}
			}	

			
				
		    
			    
		}
		turretpositions.clear();
		
	}

	public void init() {
		bg =new ScrollingBackground();
		terraingeneration();
	    getPlayer().getblueprint().getBody().setTransform(new Vector2(ShipBuilder.WIDTH*4/5,ShipBuilder.HEIGHT*7/5), 180*MathUtils.degRad);
	    detector = new CollisionDetector(player,game);
	    world.setContactListener(detector);
	    Box2DSprite.setUserDataAccessor(UserData.box2DSpriteAccesor);
	}
	
public  void terraingeneration() {
	for (int i = 0; i < 1000; i++) {
		Asteroid asteroid = new Asteroid(game,world);
	}
		
		
	}
	
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public World getWorld() {
		return world;
	}

	public ArrayList<Fixture> getTodestroy() {
		return todestroy;
	}

	public void setTodestroy(ArrayList<Fixture> todestroy) {
		this.todestroy = todestroy;
	}
	
	public void setTomine(ArrayList<Fixture> tomine) {
		this.tomine = tomine;
	}

	

	
	
}
