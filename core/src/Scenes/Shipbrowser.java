package Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.hgs.ShipBuilder;
import Screens.MenuScreen;
import model.Ship;
import model.ShipStorage;

public class Shipbrowser extends Window{
	ShipBuilder sb;
	ShipStorage storage;
	Button closebtn= new Button();
	Skin closeskin = new Skin(new TextureAtlas("closebtn.pack"));
	ButtonStyle closebtnstyle= new ButtonStyle();
	Window w;
	FileHandle selected;
	public List<String> shiplist;
    
	public Shipbrowser(final ShipBuilder sb,String title, Skin skin) {
		super("", skin);
		w = this;
		this.sb= sb;
		this.setBounds(ShipBuilder.WIDTH/2-ShipBuilder.WIDTH/4, ShipBuilder.HEIGHT/2-ShipBuilder.HEIGHT/4, ShipBuilder.WIDTH/2, ShipBuilder.HEIGHT/2);
		this.setResizable(false);
		this.setMovable(false);
		storage = new ShipStorage(sb);
		//this.debug();
		Image backgroundimage = new Image(new Texture(Gdx.files.internal("infopanel2.png")));
		Stack browserwindow= new Stack();
	    shiplist = new List<String>(skin);
	    shiplist.addListener(new ChangeListener(){@Override
	    public void changed(ChangeEvent event, Actor actor) {
	    	if (shiplist.getSelected()!=null&&shiplist.getSelected().length()>0) {
				selected = Gdx.files.local("ships/"+shiplist.getSelected()+".ship");
			}
	    }});
		Table background = new Table(skin);
		final ScrollPane container= new ScrollPane(shiplist);
		container.setWidth(50);
		final Slider scrollbar = new Slider(0, 100, 1, true, skin);
		TextButton load= new TextButton("Load", skin);
		load.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (selected!=null) {
					sb.es.ship = new Ship(selected.nameWithoutExtension());
					sb.es.storage.shipset();
				}
			}
		});
		TextButton delete= new TextButton("Delete", skin);
		delete.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (selected!=null) {
					selected.delete();
					selected=null;
					sb.es.storage.update(shiplist);
				}
			}
		});
		scrollbar.setValue(100);
		scrollbar.getStyle().knob.setMinWidth(this.getWidth()/30);
		scrollbar.getStyle().knob.setMinHeight(this.getHeight()/15);
		scrollbar.getStyle().background.setMinWidth(this.getWidth()/30);
		scrollbar.addListener(new ChangeListener() {	
		@Override
		public void changed(ChangeEvent event, Actor actor) {
			container.setScrollPercentY(1-scrollbar.getPercent());
			container.updateVisualScroll();
			
			
		}});
		
		closebtnstyle.up = closeskin.getDrawable("close_up");
	    closebtnstyle.down = closeskin.getDrawable("close_down");
	    closebtn= new Button(closebtnstyle);
	    closebtn.addListener(new ChangeListener() {
		
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				if (sb.es.ship!=null) {
					sb.es.setIndialog(false);
					w.setVisible(false);	
				}else {
					sb.es.dispose();
					sb.setScreen(new MenuScreen(sb));
				}
				
				
			}
		});
	    
	    this.top();
	    background.add(backgroundimage).size(Value.percentWidth(.4F, this),Value.percentHeight(0.62F,this)).padLeft(this.getWidth()/25);
	    browserwindow.add(container);
	    browserwindow.addActorBefore(container,background);
	    background.scaleBy(4);
	    this.add(browserwindow).size(Value.percentWidth(.3F, this),Value.percentHeight(0.55F,this)).top().padTop(this.getHeight()/25).padLeft(this.getWidth()/35);
	    this.add(scrollbar).size(Value.percentWidth(.03F, this),Value.percentHeight(0.54F, this)).top().padTop(this.getHeight()/20).padLeft(this.getWidth()/100);
	    this.add().expandX();
	    this.add(closebtn).fill(false).top().right();
	    this.row();
	    this.add(load).top().size(this.getWidth()*0.4f, this.getHeight()*0.125f).colspan(3).left().padTop(this.getHeight()/15);
	    this.row().top().size(this.getWidth()*0.4f, this.getHeight()*0.125f).colspan(3).left().padTop(this.getHeight()/20);;
	    this.add(delete);
	    
	   
		
		
	}
	@Override
	protected void drawBackground(Batch batch, float parentAlpha, float x, float y) {
		
		super.drawBackground(batch, parentAlpha, x, y-ShipBuilder.HEIGHT/60);
		
	
	}
	public FileHandle getSelected() {
		return selected;
	}

}
