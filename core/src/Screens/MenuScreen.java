package Screens;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.hgs.ShipBuilder;


public class MenuScreen implements Screen{
	private ShipBuilder game;
	
	
	Texture background;
	Texture headline;
	Image title;
	Image bg;
	Stage stage;
	Button playbtn;
	Button editorbtn;
	Button quitbtn;
	ButtonStyle playbtnstyle;
	ButtonStyle editorbtnstyle;
	ButtonStyle quitbtnstyle;
	Skin skin;
	TextureAtlas txtatlas;
	
	
	
	




	 public MenuScreen(ShipBuilder game) {
		 background = new Texture("back.png");
		 bg = new Image(background);
		 headline = new Texture("headline.png");
		 title = new Image(headline);
		 this.game = game;
		 stage = new Stage();
		 Gdx.input.setInputProcessor(stage);
		 skin = new Skin();
		 txtatlas=  new TextureAtlas("img.pack");
	     skin.addRegions(txtatlas);
	     
		 playbtnstyle = new ButtonStyle();
		 playbtnstyle.up = skin.getDrawable("playbtn");
	     playbtnstyle.down = skin.getDrawable("playbtndown");
	     playbtn= new Button(playbtnstyle);
	     
	     editorbtnstyle = new ButtonStyle();
	     editorbtnstyle.up = skin.getDrawable("editorbtn");
	     editorbtnstyle.down = skin.getDrawable("editorbtndown");
	     editorbtn= new Button(editorbtnstyle);
	     
	     quitbtnstyle = new ButtonStyle();
	     quitbtnstyle.up = skin.getDrawable("quitbtn");
	     quitbtnstyle.down = skin.getDrawable("quitbtndown");
	     quitbtn= new Button(quitbtnstyle);
	     stage.addActor(bg);
	     title.setPosition(ShipBuilder.WIDTH/2-headline.getWidth()/2,ShipBuilder.HEIGHT-headline.getHeight()*3);
	     bg.setSize(ShipBuilder.WIDTH, ShipBuilder.HEIGHT); 
	     stage.addActor(playbtn);
	     stage.addActor(title);
	     playbtn.setPosition(ShipBuilder.WIDTH-ShipBuilder.WIDTH*2/3-playbtn.getWidth(),ShipBuilder.HEIGHT/2-playbtn.getHeight()/2);
	     stage.addActor(editorbtn);
	     editorbtn.setPosition(ShipBuilder.WIDTH/2-editorbtn.getWidth()/2, ShipBuilder.HEIGHT*1/3+editorbtn.getHeight()/2);
	     stage.addActor(quitbtn);
	     quitbtn.setPosition(ShipBuilder.WIDTH-ShipBuilder.WIDTH*1/3,ShipBuilder.HEIGHT/2-playbtn.getHeight()/2);
		 
	 	ShipBuilder.effects.bgmusic.play();
		ShipBuilder.effects.bgmusic.setPosition(0);
		ShipBuilder.effects.bgmusic.setVolume(0.3f);
		ShipBuilder.effects.bgmusic.setLooping(true);
	     
	     Gdx.input.setInputProcessor(stage);
			
			
	}
	
	@Override
	public void show() {
		playbtn.addListener(new ChangeListener() {
	        

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				dispose();
				game.gs = new GameScreen(game);
				game.setScreen(game.gs);
				
			
			}
        });
		
		editorbtn.addListener(new ChangeListener() {
	        

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				dispose();
				game.es = new EditorScreen(game);
				game.setScreen(game.es);
				
			
			
			}
        });

		quitbtn.addListener(new ChangeListener() {
    

	@Override
	public void changed(ChangeEvent event, Actor actor) {
		
		System.exit(0);
	
	}
});
	}

	@Override
	public void render(float delta) {
		stage.draw();
		
		
		
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		
		ShipBuilder.effects.bgmusic.pause();
		stage.dispose();
		
		
	}

}
