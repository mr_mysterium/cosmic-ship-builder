package model;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.hgs.ShipBuilder;

import Scenes.PartMenu;

/**
 * Die Klasse Partlistener dient als Eventlistener beim Ausw�hlen eines Teiles  und setzt das augew�hlte Teil des Editors
 */
public class Partlistener extends ChangeListener{
	
	/** Der Name des ausgew�hlten Teils*/
	String name;
	
	/** Das Men� aus dem die Teile bezogen werden */
	PartMenu pm;
	
	/** Zugriff auf die Hauptinstanz des spiels */
	ShipBuilder sb;
	
	/**
	 * Inititialisiert einen neues Partlistener mit den ben�tigten Werten
	 *
	 * @param name Der Name des ausgew�hlten Teils
	 * @param pm Das Men� aus dem die Teile bezogen werden
	 * @param sb Die Hauptinstanz des Spiels
	 */
	public Partlistener(String name,PartMenu pm,ShipBuilder sb) {
		this.sb = sb;
		this.name = name;
		this.pm = pm;
	}
	
	/** 
	 * Bei einem Druck auf eines der ausw�hlbaren Schiffsteile wird das gedr�ckte Part im Partmen� ausgew�hlt 
	 */
	@Override
	public void changed(ChangeEvent event, Actor actor ) {
		if (!sb.es.isIndialog()) {
			if (sb.es.ship != null) {
				pm.setSelectedpart(name);
				pm.setPartSelected(true);
			}
		}
		
		
		
	}

}
