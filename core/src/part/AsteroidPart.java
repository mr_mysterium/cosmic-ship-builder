package part;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.hgs.ShipBuilder;

public abstract class AsteroidPart {	
Sprite sprite;
String part;
float rotation;
int material;
int mass;
int hp;

	public AsteroidPart(String part,int material,float rotation) {
		this.material= material;
		this.rotation = rotation;
		this.part = part;
		sprite=new Sprite(ShipBuilder.gameskin.getRegion(part+"_"+String.valueOf(material)));
	}

	public String getPart() {
		return part;
	}

	public float getRotation() {
		return rotation;
	}

	public int getMaterial() {
		return material;
	}

	public Sprite getSprite() {
		return sprite;
	}
	
}
