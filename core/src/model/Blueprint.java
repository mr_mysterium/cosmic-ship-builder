package model;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.hgs.ShipBuilder;

import net.dermetfan.gdx.graphics.g2d.Box2DSprite;
import part.Part;


/**
 * Blueprint bietet die physikalische Darstellung des Spielerschiffes
 */
public class Blueprint {
 
 /** Der Skalierfaktor der Schiffsteile */
 int size= 1;
 
 /** Zugriff auf die Hauptinstanz des spiels*/
 ShipBuilder game;
 
 /** Zugriff auf die physikalische Welt von Box2d */
 World world;
 
 /** Der physikalische K�rper der aus kleineren K�rpern , den Fixtures zusammengesetzt ist */
 Body body;
 
 /** Das Array das die logischen Bestandteile des Schiffes enth�lt */
 Part[][] parts;
 
 /** Die Definition des physikalischen K�rpers */
 BodyDef bodydef;
 
 /** Winkel des physikalischen K�rpers*/
 public int angle = 0;
 
    /** Der physikalische Teilk�rper der zusammengestzt, zu einem Body wird */
	Fixture fixture;
    
    /** Eine Hilfsklasse aus der libGDX Utils library die das zeichnen von Bildern auf Fixtures vereinfacht */
    Box2DSprite box2dSprite;
    
    /** Die Definition eines dreieckigen Teilk�rpers*/
    FixtureDef trifixtureDef;
	
    /** Die Definition eines spitzen Teilk�rpers*/
	FixtureDef tipfixtureDef;
	
	/** Die Definition eines boxf�rmigen Teilk�rpers*/
	FixtureDef boxfixtureDef;
	/** Eine Polygonform die aus mehreren 2d Ortsvektoren eine konvexe Dreiecksform bildet die f�r den Teilk�rer verwendet wird */
	PolygonShape trishape;

	/** Eine Polygonform die aus mehreren 2d Ortsvektoren eine konvexe Spitzenform bildet die f�r den Teilk�rer verwendet wird */
	PolygonShape tipshape;

	/** Eine Polygonform die aus mehreren 2d Ortsvektoren eine konvexe Boxform bildet die f�r den Teilk�rer verwendet wird  */
	PolygonShape boxshape;

	/** Ein Array aus Vektoren das zur Dreiecksform wird */
	Vector2[] trivert;

	/** Ein Array aus Vektoren das zur Spitzenform wird*/
	Vector2[] tipvert;

	/** Ein Array aus Vektoren das zur Boxform wird */
	Vector2[] boxvert;
	
	/**
	 * Der Konstruktor des Schiffst initialisiert erst die Definitionen der Teilk�rper,die Polygonformen und den physikalischen Schiffk�rper
	 * bevor er zum Schluss den Tag "player" bekommt damit er sp�ter bei der Kollisionsberechnung identifiziert werden kann
	 *
	 * @param game die Hauptinstanz des Programms
	 * @param world Die physikalische Welt in der der Asteroid platziert werden soll
	 * @param parts Das logische Modell auf der das physikalische Modell basieren soll
	 */
 public Blueprint(ShipBuilder game,World world, Part[][] parts) {
	this.game = game;
	this.world = world;
	this.parts = parts;
	bodydef = new BodyDef();
	bodydef.type = BodyType.DynamicBody;
	bodydef.angularDamping= 0;
	body= world.createBody(bodydef);
	body.setLinearDamping(0.7f);
	
	
	trifixtureDef = new FixtureDef();
	trifixtureDef.density =1;
	trifixtureDef.filter.categoryBits = Box2dFILTER.BIT_PART;
	trifixtureDef.filter.maskBits = Box2dFILTER.BIT_ASTERODIPART;
    tipfixtureDef = new FixtureDef();
    tipfixtureDef.density = 1;
    tipfixtureDef.filter.categoryBits = Box2dFILTER.BIT_PART;
	tipfixtureDef.filter.maskBits = Box2dFILTER.BIT_ASTERODIPART;
	boxfixtureDef = new FixtureDef();
	boxfixtureDef.density=1;
	boxfixtureDef.filter.categoryBits = Box2dFILTER.BIT_PART;
	boxfixtureDef.filter.maskBits = Box2dFILTER.BIT_ASTERODIPART;
	
	trishape = new PolygonShape();
	tipshape = new PolygonShape();
	boxshape = new PolygonShape();
	
	trivert = new Vector2[3];
	tipvert = new Vector2[3];
	boxvert = new Vector2[4];
	
	materialize();
	body.setUserData("player");
	 
}
 
 /**
  * Der physikalische K�rper wird aus den einzelnen logischen Teilen materialisiert und ruft dabei immer die Methode auf die ein neues Modul
  * zum physikalischen Schiffsk�rper hinzuf�gt
  */
 public void materialize() {
	 for (int i = 0; i < parts.length; i++) {
			for (int j = 0; j < parts[i].length; j++) {
				if (parts[i][j] != null) {
					parts[i][j].getSprite().setPosition(0, 0);
					box2dSprite= new Box2DSprite(parts[i][j].getSprite());
					addmodule(parts[i][j], i, j);
				}
			}
		} 
 }
 
	/**
	 * F�gt Schrittweise durch Angabe einer Position und dem hinzuzuf�gendem Teil alle logischen Teile zum Schiffsk�rper hinzu 
	 *
	 * @param p Das hinzuzuf�gende Teil
	 * @param gridposx Die xPosition des Teils in dem Modell und Physikk�rper
	 * @param gridposy Die yPosition des Teils in dem Modell und Physikk�rper
	 */
	private void addmodule(Part p, int gridposx, int gridposy) {
		int x = gridposx;
		int y = gridposy;
		if (p.getPart().contains("dreieck")) {
			trivert[0] = new Vector2(x*size,y*size);
			trivert[1] = new Vector2(x*size+size,y*size);
			trivert[2] = new Vector2(x*size,y*size+size);
			float originx = trivert[0].x+size/2f;
			float originy = trivert[0].y+size/2f;
		float angle =(float) ((270-p.getRotation())); 
			for (int i = 0; i < trivert.length; i++) {
				trivert[i] = UTILITY.rotatevec(trivert[i], angle,originx,originy);
			
			}
			
			trishape.set(trivert);
			trifixtureDef.shape = trishape;
			fixture = body.createFixture(trifixtureDef);
			box2dSprite.setRotation(angle);
			UserData userData = new UserData();
			userData.sprite =box2dSprite;
			userData.metadata =  new Vector2(gridposx, gridposy);
			fixture.setUserData(userData);
			
		} else if (p.getPart().contains("spitze")) {
			tipvert[0] = new Vector2(x*size,y*size);
			tipvert[1] = new Vector2(x*size+size, y*size);
			tipvert[2] = new Vector2(x*size+size/2f, y*size+size);
			float angle =(float) ((180-p.getRotation())); 
			float originx = tipvert[0].x+size/2f;
			float originy = tipvert[0].y+size/2f;
			for (int i = 0; i < tipvert.length; i++) {
				tipvert[i] = UTILITY.rotatevec(tipvert[i], angle,originx,originy);
			
			}
			tipshape.set(tipvert);
			tipfixtureDef.shape = tipshape;
			fixture = body.createFixture(tipfixtureDef);
			box2dSprite.setRotation(angle);
			UserData userData = new UserData();
			userData.sprite =box2dSprite;
			userData.metadata = new Vector2(gridposx, gridposy);
			fixture.setUserData(userData);
			
		} else {
			boxvert[0] = new Vector2(x*size,y*size);
			boxvert[1] = new Vector2(x*size+size, y*size);
			boxvert[2] = new Vector2(x*size+size, y*size+size);
			boxvert[3] = new Vector2(x*size, y*size+size);
			float angle =(float) ((-p.getRotation()+180)); 
			float originx = boxvert[0].x+size/2f;
			float originy = boxvert[0].y+size/2f;
			for (int i = 0; i < boxvert.length; i++) {
				boxvert[i] = UTILITY.rotatevec(boxvert[i], angle,originx,originy);
			
			}
			boxshape.set(boxvert);
			boxfixtureDef.shape = boxshape;
			fixture = body.createFixture(boxfixtureDef);
			box2dSprite.setRotation(angle);
			UserData userData = new UserData();
			userData.sprite =box2dSprite;
			userData.metadata =  new Vector2(gridposx, gridposy);
			fixture.setUserData(userData);
		}
	}

/**
 * Gibt den physikalischen K�rper des Schiffs zur�ck
 *
 * @return the body
 */
public Body getBody() {
	return body;
}

/**
 * Detzt den physikalischen K�rper auf den angegebenen
 *
 * @param body der neue K�rper
 */
public void setBody(Body body) {
	this.body = body;
}

 
}
