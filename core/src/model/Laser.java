package model;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.glutils.GLVersion.Type;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class Laser {
public static ArrayList<Laser>laserlist= new ArrayList<Laser>();
public static boolean active=false;
World w;
Body laserbody;
BodyDef laserbodydef;
Fixture laserfixture;
FixtureDef laserfixturedef;
Vector2[]laservert;
PolygonShape lasershape; 
float width;
float range;
int material;

public Laser(World w,int material) {
	this.w = w;
	this.material=material-1;
	width=0.5f;
	range=10*material;
	laserbodydef = new BodyDef();
	laserbodydef.type = BodyType.KinematicBody;
	laserbody= w.createBody(laserbodydef);
	laserfixturedef = new FixtureDef();
	laserfixturedef.filter.categoryBits = Box2dFILTER.BIT_LASER;
	laserfixturedef.filter.maskBits = Box2dFILTER.BIT_ASTERODIPART;
	lasershape = new PolygonShape();
	laservert = new Vector2[4];
	laservert[0] = new Vector2(-width/2,0f);
	laservert[1] = new Vector2(width/2, 0f);
	laservert[2] = new Vector2(width/2, -range);
	laservert[3] = new Vector2(-width/2,-range);
	lasershape.set(laservert);
	laserfixturedef.shape = lasershape;
	lasershape.dispose();
	laserfixture = laserbody.createFixture(laserfixturedef);
	Turretdata data=Effects.beamdata.get(material);
	data.setLaser(this);
	laserfixture.setUserData(data);
	laserbody.setUserData("laser");
	laserlist.add(this);
	
}

public void update(Vector2 pos,float rotation,Vector2 mousepos){
	float dist= Vector2.dst(pos.x, pos.y, mousepos.x, mousepos.y);
	laserbody.destroyFixture(laserfixture);
	lasershape = new PolygonShape();
	laservert[0] = new Vector2(-width/2,0f);
	laservert[1] = new Vector2( width/2,0f);
	
		if (dist<range) {
			laservert[2] = new Vector2( width/2,-(Vector2.dst(pos.x, pos.y, mousepos.x, mousepos.y)));
			laservert[3] = new Vector2(-width/2,-(Vector2.dst(pos.x, pos.y, mousepos.x, mousepos.y)));
		}else {
			laservert[2] = new Vector2( width/2,-(range));
			laservert[3] = new Vector2(-width/2,-(range));
		
		
		
	}
	lasershape.set(laservert);
	laserfixturedef.shape = lasershape;
	lasershape.dispose();
	laserfixture = laserbody.createFixture(laserfixturedef);
	laserfixture.setUserData(Effects.beamdata.get(material));
	laserbody.setTransform(pos,(float) ((Math.atan2 (mousepos.x - pos.x, -(mousepos.y - pos.y)))));
	}
	

public Body getLaserbody() {
	return laserbody;
}

public void setLaserbody(Body laserbody) {
	this.laserbody = laserbody;
}

public Fixture getLaserfixture() {
	return laserfixture;
}

public void setLaserfixture(Fixture laserfixture) {
	this.laserfixture = laserfixture;
}


}
