package model;

import java.util.Random;

import part.AsteroidPart;
import part.Stein;
import part.Steinkante;
import part.Steinspitze;

/**
 * Die bereits vordefinierten Formen f�r Asteroiden
 */
public class TEMPLATE {
	
	/** Der gro�e  generierte Asteroid */
	public static AsteroidPart[][] composite;
	
	/**
	 * Erstelle einen Asteroiden durch zuf�llige generierung mit einem Algorithmus , indem drei rechtecke verschiedener gr��e auf einem 5x5 Feld platziert werden.
	 * Danach werden flache kannten gegl�ttet und abgerundet welches den Asteroiden deutlich nat�rlicher aussehen l�sst
	 *
	 * @param size die gr��e
	 * @param material das Material
	 */
	public static void createcomposite(int size, int material) {
		if (size==1) {
			composite= new AsteroidPart[5][5];
			placerect(3,material);
			placerect(2,material);
			placerect(2,material);
			for (int i = 0; i < 4; i++) {
				smooth(material);
				rotatecomp();	
			}
			slope(material);
			
		}else {
			
		}
	}

	/**
	 * Platziere ein Rechtechk in einem 5x5 Feld
	 *
	 * @param size die Gr��e
	 * @param material das Material
	 */
	private static void placerect(int size,int material) {
		Random r = new Random();
		int offmax = composite.length-size;
		int offx= r.nextInt(offmax+1);
		int offy= r.nextInt(offmax+1);
		for (int i = 0; i < composite.length; i++) {
			for (int j = 0; j <composite[i].length; j++) {
				if (i>=offx&&j>=offy&&i<offx+size&j<offy+size) {
					composite[i][j]= new Stein("stein",material, 0);
				}
			}
		} 
	}
	
	/**
	 * Gl�tten flacher Kanten  mit einer L�nge von mehr als 3 Teilen, durch abflachen und wiederholtes rotieren
	 *
	 * @param material Das Material
	 */
	private static void smooth(int material) {
			
			
				int length=0;
				int start=-1;
				
				 for (int i =composite.length-1; i>=0; i--) {
						for (int j =  0; j <composite[i].length; j++) {
							if (composite[j][i] != null) {
								if (start==-1)start=j;
								length++;
							}else {
							}
						}
						System.out.println("\n");
						if (length>=3) {
							Random r = new Random();
							int stonenum= 1+r.nextInt(2);
							int off=start+r.nextInt(length-stonenum+1);
							for (int k = 0; k < composite[i].length; k++) {
								if (composite[k][i]!=null) {
								}else {
								}
								composite[k][i]=null;
							}
						
							for (int k = off; k < off+stonenum; k++) {
								composite[k][i]=new Stein("stein",material, 0);	
							}
						}
						if (length>0) {
							return;
						}	
					} 
				 
			}
			
			 
		
		
			
		
		
	
	/**
	 * Das Rotieren des Asteroides damit die pberste reihe bearbeitet werden kann
	 */
	private static void rotatecomp() {
		int size = composite.length;
		 AsteroidPart[][] ret = new AsteroidPart[size][size];
		 for (int i = 0; i < size; ++i) 
		  for (int j = 0; j < size; ++j) 
		   ret[i][j] = composite[size - j - 1][i]; //***

		composite = ret;
	}
	
	/**
	 * Eine  abgekantete oberfl�che erzeugen
	 *
	 * @param material Das Material
	 */
	private static void slope(int material) {
		for (int i = 0; i < composite.length; i++) {
			for (int j = 0; j <composite[i].length; j++) {
				
					if(composite[i][j]==null&&(i>0&&j>0)&&composite[i-1][j]!=null&&composite[i][j-1]!=null&&composite[i-1][j].getPart().equals("stein")&&composite[i][j-1].getPart().equals("stein"))composite[i][j]= new Steinkante("steinkante",material, 270);
					if(composite[i][j]==null&&(i<composite.length-1&&j>0)&&composite[i][j-1]!=null&&composite[i+1][j]!=null&&composite[i][j-1].getPart().equals("stein")&&composite[i+1][j].getPart().equals("stein"))composite[i][j]= new Steinkante("steinkante",material, 0);
					if(composite[i][j]==null&&(i<composite.length-1&&j<composite[i].length-1)&&composite[i+1][j]!=null&&composite[i][j+1]!=null&&composite[i+1][j].getPart().equals("stein")&&composite[i][j+1].getPart().equals("stein"))composite[i][j]= new Steinkante("steinkante",material, 90);
					if(composite[i][j]==null&&(i>0&&j<composite[i].length-1)&&composite[i][j+1]!=null&&composite[i-1][j]!=null&&composite[i][j+1].getPart().equals("stein")&&composite[i-1][j].getPart().equals("stein"))composite[i][j]= new Steinkante("steinkante",material, 180);	
			}
		} 
	}

	/**
	 * Einen vordefinierten runden Asteroid hinzuf�gen
	 *
	 * @param size Die gr��e des Asteroiden
	 * @param material Das Material
	 * @param posx Die XPosisition
	 * @param posy Die YPostion
	 * @return Das Asteroidenteil an der bestimmen position
	 */
	public static AsteroidPart addround(int size, int material, int posx, int posy) {
		AsteroidPart[][] round = new AsteroidPart[][] {
				{ new Steinkante("steinkante",material, 90), new Stein("stein",material, 0), new Steinkante("steinkante",material, 0) },
				{ new Stein("stein",material, 0), new Stein("stein",material, 0), new Stein("stein",material, 0) },
				{ new Steinkante("steinkante",material, 180), new Stein("stein",material, 0), new Steinkante("steinkante",material, 270) } };

		return round[posx][posy];

	}

	/**
	 * Adddiagonal.
	 *
	 * @param size the size
	 * @param material the material
	 * @param posx the posx
	 * @param posy the posy
	 * @return the asteroid part
	 */
	public static AsteroidPart adddiagonal(int size, int material, int posx, int posy) {
		AsteroidPart[][] diagonal = new AsteroidPart[][] {
				{ new Stein("stein",material, 90), new Steinkante("steinkante",material, 0), null },
				{ new Steinkante("steinkante",material, 180), new Stein("stein",material, 0), new Steinkante("steinkante",material, 0) },
				{ null, new Steinkante("steinkante",material, 180), new Stein("stein",material, 270) } };

		return diagonal[posx][posy];

	}

	/**
	 * Addspikey.
	 *
	 * @param size the size
	 * @param material the material
	 * @param posx the posx
	 * @param posy the posy
	 * @return the asteroid part
	 */
	public static AsteroidPart addspikey(int size, int material, int posx, int posy) {
		AsteroidPart[][] spikey = new AsteroidPart[][] {
			    {null,new Steinspitze("steinspitze",material,0),null },
				{new Steinspitze("steinspitze",material,90),new Stein("stein",material,0),new Steinspitze("steinspitze",material,270) },
				{null,new Steinspitze("steinspitze",material,180),null} };

		return spikey[posx][posy];

	}
	
	/**
	 * Addcomposite.
	 *
	 * @param size the size
	 * @param material the material
	 * @param posx the posx
	 * @param posy the posy
	 * @return the asteroid part
	 */
	public static AsteroidPart addcomposite(int size, int material, int posx, int posy) {
		return composite[posx][posy];
		

	}
	
}
