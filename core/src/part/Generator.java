package part;

import Scenes.PartMenu;
import model.Ship;

public class Generator extends Part{
int gen;
	public Generator(PartMenu pm, Ship ship, String part, int material,int gridx,int gridy, float rot) {
		super(pm, ship, part, material, gridx, gridy, rot, null);
		switch (material) {
		case 1:
			gen=50;
			mass=5;
			hpmax=5;
			break;
		case 2:
			gen=100;
			mass=10;
			hpmax=10;				
			break;

		case 3:
			gen=200;
			mass=15;
			hpmax=15;
			break;

		case 4:
			gen=200;
			mass=30;
			hpmax=30;
			break;
		}
		hp = hpmax;
	}
	@Override
	public void add(Ship ship, int material) {
		
		ship.setEnergyproduction(ship.getEnergyproduction()+gen);
		ship.setMass(ship.getMass()+mass);
		ship.setHp(ship.getHp()+hpmax);
		
		
	}
	
}
