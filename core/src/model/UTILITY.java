package model;

import java.util.Random;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import part.Part;

// TODO: Auto-generated Javadoc
/**
 * The Class UTILITY.
 */
public class UTILITY {
	
	/**
	 * Rotatevec.
	 *
	 * @param v the v
	 * @param angle the angle
	 * @param originx the originx
	 * @param originy the originy
	 * @return the vector 2
	 */
	public static Vector2 rotatevec(Vector2 v, double angle,float originx, float originy) {
	   
	    Vector2 vec= v;
	    angle = angle*Math.PI/180;
	    float new_x = (float) ((vec.x-(originx)) * Math.cos(angle) - (vec.y-(originy)) * Math.sin(angle) + originx);
	    float new_y = (float) ((vec.x-(originx)) * Math.sin(angle) + (vec.y-(originy)) * Math.cos(angle) + originy);
	    vec.x = new_x;
	    vec.y = new_y;
	    return vec;
	}
	
	/**
	 * Rotate part.
	 *
	 * @param array the array
	 * @return the part[][]
	 */
	public static Part[][] rotatePart(Part[][]array) {
		int size = array.length;
		 Part[][] ret = new Part[size][size];
		 for (int i = 0; i < size; ++i) 
		  for (int j = 0; j < size; ++j) 
		   ret[i][j] = array[size - j - 1][i]; //***

		return ret;
	}
	
	/**
	 * Gets the linear random number.
	 *
	 * @param maxSize the max size
	 * @return the linear random number
	 */
	public static int getLinearRandomNumber(int maxSize){
	    int randomMultiplier = maxSize * (maxSize + 1) / 2;
	    Random r=new Random();
	    int randomInt = r.nextInt(randomMultiplier);

	    int linearRandomNumber = 0;
	    for(int i=maxSize; randomInt >= 0; i--){
	        randomInt -= i;
	        linearRandomNumber++;
	    }

	    return linearRandomNumber;
	}
	
	

}
