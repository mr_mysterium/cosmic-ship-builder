package part;

import Scenes.PartMenu;
import model.Ship;

public class Spitze extends Part{
	
	public Spitze(PartMenu pm, Ship ship, String part, int material,int gridx,int gridy, float rot) {
		super(pm, ship, part, material, gridx, gridy, rot, null);
		switch (material) {
		case 1:
			mass=10;
			hpmax=20;
			break;
		case 2:
			
			mass=20;
			hpmax=40;				
			break;

		case 3:
			
			mass=30;
			hpmax=60;
			break;

		case 4:
			
			mass=60;
			hpmax=120;
			break;
		}
		hp = hpmax;
	}
	@Override
	public void add(Ship ship, int material) {
		ship.setMass(ship.getMass()+mass);
		ship.setHp(ship.getHp()+hpmax);
		
	}
	
	
}
