package model;

import java.util.Random;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.hgs.ShipBuilder;

/**
 * Klasse die den Hintergund des Spieles definiert
 */
public class ScrollingBackground {
	
	/**Skalierungsfaktor */
	int scaling=5;
	
	/** Hintergrundsterne */
	Texture stars;
	
	/** Die kosmischen Nebel */
	Texture[]nebulas;
	
	/** Positionen der Nebel */
	Vector2[]nebulapositions;
	
	/**
	 * Erzeugt einen neuen Hintergund
	 */
	public ScrollingBackground() {
		stars= new Texture("stars.png");
		nebulas = new Texture[6];
		
		nebulapositions= new Vector2[]{new Vector2(ShipBuilder.WIDTH*2/scaling,ShipBuilder.HEIGHT*2/scaling),new Vector2(ShipBuilder.WIDTH*6/scaling,ShipBuilder.HEIGHT*8/scaling),new Vector2(ShipBuilder.WIDTH*5/scaling,ShipBuilder.HEIGHT*11/scaling),new Vector2(ShipBuilder.WIDTH*8/scaling,ShipBuilder.HEIGHT*2/scaling),new Vector2(ShipBuilder.WIDTH*5/scaling,ShipBuilder.HEIGHT*3/scaling),new Vector2(ShipBuilder.WIDTH*2/scaling,ShipBuilder.HEIGHT*6/scaling),};
		for (int i = 0; i < nebulas.length; i++) {
			nebulas[i] = new Texture("nebula"+i+".png");
			
		}
	}
	
	/**
	 * Zeichne den Hintergund mit den Nebeln und den Sternen
	 * @param batch Die Sammlung aller zu zeichnenden Grafiken
	 */
	public void draw(SpriteBatch batch) {
		batch.begin();
		
		for (int i = 0; i <8; i++) {
			for (int j = 0; j <12; j++) {
				batch.draw(stars, ShipBuilder.WIDTH*i/scaling,ShipBuilder.HEIGHT*j/scaling,stars.getWidth()/scaling,stars.getHeight()/scaling);
			}
		}
		for (int i = 0; i < nebulas.length; i++) {
			batch.draw(nebulas[i], nebulapositions[i].x,nebulapositions[i].y,nebulas[i].getWidth()/scaling,nebulas[i].getHeight()/scaling);
		}
		batch.end();
	}
	
}
