package part;

import Scenes.PartMenu;
import model.Ship;

public class Panzerung extends Part{

	public Panzerung(PartMenu pm, Ship ship, String part, int material,int gridx,int gridy, float rot) {
		super(pm, ship, part, material, gridx, gridy, rot, null);
		switch (material) {
		case 1:
			mass=15;
			hpmax=30;
			break;
		case 2:
			
			mass=30;
			hpmax=60;				
			break;

		case 3:
			
			mass=45;
			hpmax=90;
			break;

		case 4:
			
			mass=90;
			hpmax=180;
			break;
		}
		hp = hpmax;
	}

	@Override
	public void add(Ship ship, int material) {
		ship.setMass(ship.getMass()+mass);
		ship.setHp(ship.getHp()+hpmax);
		
	}

}
