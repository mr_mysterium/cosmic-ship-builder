package part;

import Scenes.PartMenu;
import model.Ship;

public class Steuerung extends Part {
	int econ;
	int anglthrust;
	int breakthrust;
	public Steuerung(PartMenu pm, Ship ship, String part, int material,int gridx,int gridy, float rot) {
		super(pm, ship, part, material, gridx, gridy, rot, null);
		switch (material) {
		case 1:
			econ=10;
			anglthrust=40;
			mass=10;
			hpmax=10;
			breakthrust=50;
			break;
		case 2:
			econ=20;
			anglthrust=80;
			mass=20;
			hpmax=20;
			breakthrust=100					;
			break;

		case 3:
			anglthrust=120;
			econ=40;
			mass=30;
			hpmax=30;
			breakthrust=200;
			break;

		case 4:
			anglthrust=120;
			econ=40;
			mass=60;
			hpmax=60;
			breakthrust=175;
			break;
		}
		hp = hpmax;
	}

	@Override
	public void add(Ship ship, int material) {
		ship.setEnergyconsumption(ship.getEnergyconsumption()+econ);
		ship.setMass(ship.getMass()+mass);
		ship.setHp(ship.getHp()+hpmax);
		ship.setBreakthrust(ship.getBreakthrust()+breakthrust);
		ship.setAngularthrust(ship.getAngularthrust()+anglthrust);
		
	}
	

}
