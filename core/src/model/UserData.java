package model;

import net.dermetfan.gdx.graphics.g2d.Box2DSprite;
import net.dermetfan.gdx.physics.box2d.Breakable;

import com.badlogic.gdx.math.Vector2;

import net.dermetfan.*;
import net.dermetfan.utils.Function;


/**
 * Selbsterstelle Klasse die als Datenhalter in den Teilkörpern existiert
 */
public class UserData {
	   
   	/** The sprite. */
   	public Box2DSprite sprite;
	   
   	/** Die positionsdata */
   	public Vector2 metadata;
	   
	   /** The Constant box2DSpriteAccesor. */
   	public static final Function<Object, Box2DSprite> box2DSpriteAccesor= new Function<Object, Box2DSprite>() {

		@Override
		public Box2DSprite apply(Object data) {
			if(data instanceof UserData) 
	            return ((UserData) data).sprite;
	         else if(data instanceof Box2DSprite)
	            return (Box2DSprite) data;
			return null;
		}
	};

	


	}