package model;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.scenes.scene2d.utils.TransformDrawable;
import com.badlogic.gdx.utils.Array;
import com.hgs.ShipBuilder;

/**
 * Die Klasse CollisionDetector dient als Wekzeug zur Bestimmung und Sendung von Events die von Kollisionen ausgel�st werden
 */
public class CollisionDetector implements ContactListener{
	
	/** Der Spieler*/
	Player player;
	ShipBuilder game;
	/**
	 * Erzeugt einen neuen Kollisionsdetektor
	 *
	 * @param player Der Spieler
	 */
	public CollisionDetector(Player player,ShipBuilder game) {
		this.player = player;
		this.game = game;
	}
	
	/** 
	 * Hier  k�nnen Asteroiden mit Schiffsteilen und Schiffsteilen mit Asteroiden kollidieren. Je nachdem welche Kollision zuerst erkannt werden die Fixtures mit FixtureA oder FixtureB gekennzeichnet was die �berpr�fung durch UserdataTags die in  {@link Blueprint} und {@link Asteroid} gesetzt wurden m�glich macht
	 */
	@Override
	public void beginContact(Contact contact) {
		if (player!=null) {
			
			if (contact.getFixtureA().getBody().getUserData().equals("player")&&contact.getFixtureB().getBody().getUserData().equals("asteroid"))player.collide(contact.getFixtureA().getBody().getLinearVelocity(),contact.getFixtureB().getBody().getMass(),contact.getWorldManifold(),contact.getFixtureA());
	     	if (contact.getFixtureB().getBody().getUserData().equals("player")&&contact.getFixtureA().getBody().getUserData().equals("asteroid"))player.collide(contact.getFixtureB().getBody().getLinearVelocity(),contact.getFixtureA().getBody().getMass(),contact.getWorldManifold(),contact.getFixtureB());
	     	if (contact.getFixtureB().getBody().getUserData().equals("laser")&&contact.getFixtureA().getBody().getUserData().equals("asteroid")) {
	     		Array<Fixture>fixtures= new Array<Fixture>();
	     		fixtures = contact.getFixtureA().getBody().getFixtureList();
	     		ArrayList<Fixture>destructor= new ArrayList<Fixture>();
	     		for (int i=0;i<fixtures.size;i++) {
	     			destructor.add(fixtures.get(i));
	     		}
	     		game.gs.setTomine(destructor);
	     		
	     	}
	     	if (contact.getFixtureA().getBody().getUserData().equals("laser")&&contact.getFixtureB().getBody().getUserData().equals("asteroid")) {
	     		Array<Fixture>fixtures= new Array<Fixture>();
	     		fixtures = contact.getFixtureB().getBody().getFixtureList();
	     		ArrayList<Fixture>destructor= new ArrayList<Fixture>();
	     		for (int i=0;i<fixtures.size;i++) {
	     			destructor.add(fixtures.get(i));
	     		}
	     		game.gs.setTomine(destructor);
	     		
	     	}
				
			
		}
		
	}
	
	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		if (contact.getFixtureA().getBody().getUserData().equals("laser")&&contact.getFixtureB().getBody().getUserData().equals("asteroid")||contact.getFixtureB().getBody().getUserData().equals("laser")&&contact.getFixtureA().getBody().getUserData().equals("asteroid"))contact.setEnabled(false);
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}

}
